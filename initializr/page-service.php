<?php get_header(); ?>

<main>

<section class="relative topStaff">
	<div class="container">
		<div class="bgImg pageFvImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_service_fv.jpg');" data-aos="fade-right"></div>
	<div class="pageFvBoxWrap topFvBoxWrap text-center" data-aos="fade-left">
		<div class="topFvBoxText">
			<div class="text-center">
				<p class="fontEn h3 titleLine mb10">Service</p>
				<h2 class="h3 mb10">取扱保険・サービス</h2>
			</div>
		</div>
	</div>
	</div>
</section>

<section class="topAbout margin" id="consumer">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<p class="fontEn h3 titleLine mb10">Consulting</p>
				<h3 class="h3 bold mb10 mainColor">コンサルティング</h3>
				<h4 class="bold h4 mb10">ライフプランニングのサポート</h4>
			</div>
			<div class="col-sm-6">
				<p>菊池保険サービスのお客様パートナーは、保険だけでなく金融、税務などに関する幅広い知識も兼ね備え、お客様の一生涯のパートナーでありたいという志を持って活動しています。</p>
				<p>お客様を取り巻く環境が常に変化する時代だからこそ、重要なライフプランニングや保障の設計。プロと一緒にじっくり考えてみませんか？</p>
			</div>
		</div>
		<div class="row mb30" data-aos="fade-up">
			<div class="col-sm-4">
				<div class="topAboutBox">
					<img class="pageServiceIcoImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_consul_01.png" alt="保険適性診断">
					<h4 class="h4 mb10 titleBd text-center">保険適性診断</h4>
					<p class="gray text_m">新しく保険に入りたい！今の保険を見直したい！そんな方のために菊池保険サービスでは、お会いしてはじめに「保険の適性診断」を受けていただきます。その結果から私たち保険のプロがお客様にオススメの保険を提案させていただきます。</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="topAboutBox">
					<img class="pageServiceIcoImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_consul_02.png" alt="ライフプランコンサルティング">
					<h4 class="h4 mb10 titleBd text-center">ライフプランコンサルティング</h4>
					<p class="gray text_m">「結婚」したら、「妊娠・出産」したら、「マイホームを購入」したら、「子供が独立」したら、「独立・開業したら」「定年したら」、、、人生では様々なイベントがあり、その都度ライフスタイルが変わってきます。それらの一つ一つをお聞かせいただき、お客様の立てた人生設計をもとにどのような保険のプランが適切なのかをアドバイスさせていただきます。</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="topAboutBox">
					<img class="pageServiceIcoImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_consul_03.png" alt="事故後対応">
					<h4 class="h4 mb10 titleBd text-center">事故後対応</h4>
					<p class="gray text_m">万が一事故にあった場合はこちらのメモをご利用ください。また、もしもの時のために印刷して車に乗せておくことをオススメします。</p>
				</div>
			</div>
		</div>
		<div class="row mb30" data-aos="fade-up">
			<div class="col-sm-4">
				<div class="topAboutBox">
					<img class="pageServiceIcoImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_consul_04.png" alt="その他">
					<h4 class="h4 mb10 titleBd text-center">その他</h4>
					<p class="gray text_m">年金相談<br>役員・従業員退職金制度<br>企業年金制度<br>事業継承<br>相続対策</p>
				</div>
			</div>
			<!--
			<div class="col-sm-4">
				<div class="topAboutBox">
					<img class="pageServiceIcoImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_service_ico_02.png" alt="">
					<h4 class="h4 mb10 titleBd text-center">自動車保険</h4>
					<p class="gray text_m">火災や風水害などの自然災害から日常生活の思いもよらないリスクまで、大切な家（建物）や財産（家財道具など）を守ります。</p>
				</div>
			</div>
			-->
		</div>
	</div>
</section>
<section class="topAbout margin" id="company">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<p class="fontEn h3 titleLine mb10">Custom</p>
				<h3 class="h3 bold mb10 mainColor">保険づくりのサポート</h3>
				<h4 class="bold h4 mb10">オーダーメイド保険のつくりかた</h4>
			</div>
			<div class="col-sm-6 mb30">
				<p>お客様によって年齢・性別・家族構成などが全く異なるように、お客様の数だけ保険のつくりかたがあります。菊池保険サービスではお客様にヒアリングをさせていただき、取り扱い保険会社の中から最も適したものを組み合わせてご提案するので「完全オーダーメイド」の保険をつくることが可能です。</p>
			</div>
		</div>
		<div class="text-center">
			<h4 class="h3 mb10 titleBd inlineBlock mb50">生命保険</h4>
		</div>
		<div class="row mb30" data-aos="fade-up">
			<div class="col-sm-4">
				<div class="topAboutBox">
					<img class="pageServiceIcoImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_custom_01.png" alt="生命保険">
					<h4 class="h4 mb10 titleBd text-center">生命保険</h4>
					<p class="gray text_m">私たちは、病気や事故、災害などにより、いつ死亡したり入院したりするかわかりません。
一方、長生きした場合でも十分な収入があるとは限りません。この様に私たちは一生を通じて、常に収支のバランスが崩される危険にさらされているのです。収支のバランスが崩れると、ご自分や家族の生活まで壊してしまいます。
そんなリスクからお客様の大切なものや、大切な人をお守りできる保険です。</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="topAboutBox">
					<img class="pageServiceIcoImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_custom_02.png" alt="けがの保険">
					<h4 class="h4 mb10 titleBd text-center">けがの保険</h4>
					<p class="gray text_m">日常生活はもちろん、お仕事中、くるまに搭乗中、予測できない事故でおけがをしてしまうこともあります。治療に伴う入院費・通院費は思った以上に費用がかかり、家計の負担になってしまいます。
けがの保険は、そんな日々の生活に潜むリスクに備える保険です。</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="topAboutBox">
					<img class="pageServiceIcoImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_custom_03.png" alt="医療・介護の保険">
					<h4 class="h4 mb10 titleBd text-center">医療・介護の保険</h4>
					<p class="gray text_m">医学の進歩により、一昔前に比べ平均余命は延びています。病気やけがで入院したときの保障である「医療保険」や一定の要介護状態になってしまったときに備える「介護保険」は今やなくてはならない生活必需品といえます。
「生きるため」の保障としてしっかりとした内容設計をさせていただきます。</p>
				</div>
			</div>
		</div>
		<div class="text-center">
			<h4 class="h3 mb10 titleBd inlineBlock mb50">損害保険</h4>
		</div>
		<div class="row mb30" data-aos="fade-up">
			<div class="col-sm-4">
				<div class="topAboutBox">
					<img class="pageServiceIcoImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_custom_04.png" alt="くるまの保険">
					<h4 class="h4 mb10 titleBd text-center">くるまの保険</h4>
					<p class="gray text_m">くるまは便利で身近な移動手段として今では生活に欠かせないものになっています。
その一方、交通事故というリスクが生まれ、そのリスクは我々の生活の身近に存在することを忘れてはいけません。「自動車保険」はそのリスクに備える重要な役割を果たす保険です。万一のリスクに備え充実した補償内容を設計し、ご提案します。</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="topAboutBox">
					<img class="pageServiceIcoImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_custom_05.png" alt="火災保険">
					<h4 class="h4 mb10 titleBd text-center">火災保険</h4>
					<p class="gray text_m">近年の火災保険は「火事で家が焼けてしまったとき」だけでなく、水災や盗難など幅広いリスクに備えることが出来る保険です。
家を失うということは、永年かかって築いた生活・活動の基盤を失うことです。
未曾有の自然災害も増加している近年、内容をしっかり見直し充実した補償を備えるお手伝いをさせていただきます。</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="topAboutBox">
					<img class="pageServiceIcoImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_custom_06.png" alt="企業を取り巻く保険">
					<h4 class="h4 mb10 titleBd text-center">企業を取り巻く保険</h4>
					<p class="gray text_m">企業の活動にはさまざまなリスクがつきものです。社用車で人をはねてしまった、PL事故を起こしてしまった、火災が発生して工場が稼働停止になったが従業員様の当面のお給料を確保しなければならない、などリスクを上げるときりがありません。
どのような保険が必要で、どのような設計内容が最適なのかを診断・分析することで無駄を省きコスト削減を実現することが出来るかもしれません。先ずはご相談ください。</p>
				</div>
			</div>
		</div>
	</div>
</section>



<section class="topService margin" id="consulting">
	<div class="container">
		<div class="padding bgWhite">
			<div class="text-center">
				<p class="fontEn h3 titleLine mb10">Management</p>
				<h3 class="h3 bold mb30 mainColor">企業保険管理サービス</h3>
			</div>
			<div class="width720 mb50">
				<p>貴社の保険証券をお預かりし、当社にて「保険の棚卸」をおこないます。</p>
				<p>保険加入状況を把握し、「経営者の想いと合致しているのか（想いの実現）」「企業として必要な保障に加入しているのか（リスクマネジメント）」「会社の規模や業種にマッチングしない無駄な保険加入で収益を圧迫していないか（収益改善）」などをプロとして公正な目で診断し、貴社にとって最適なアドバイスを行います。</p>
				<p>その後、保険管理を当社が代行し、貴社の業務負担を軽減、適正な保険マネジメントを行います。</p>
			</div>
			<!--
			<div class="pageCommonRecommendBox width720 mb50" data-aos="fade-up">
				<ul class="LiInlineBlock h5">
					<li><i class="fa fa-check-circle"></i><span>亡くなった父親の手書きの遺言書を発見しました。勝手に開けても良いのでしょうか？</span></li>
					<li><i class="fa fa-check-circle"></i><span>夫が亡くなりましたが、子どもはいません。私以外で誰が相続人になるのでしょうか？</span></li>
					<li><i class="fa fa-check-circle"></i><span>亡くなった祖父の名義になっている不動産がありました。このまま、放っておいても大丈夫なのでしょうか？</span></li>
					<li><i class="fa fa-check-circle"></i><span>亡くなった父親に多額の借金があることが分かりました。払わないといけないのでしょうか？</span></li>
					<li><i class="fa fa-check-circle"></i><span>自分の子どもに名義変更をしたいと考えています。具体的にはどんな手続きでしょうか？</span></li>
					<li><i class="fa fa-check-circle"></i><span>会社を新たに設立したいのですが、登記の仕方が分からない　　など</span></li>
				</ul>
			</div>
			-->
			<div class="text-center">
				<h4 class="h3 mb10 titleBd inlineBlock mb50">企業保険管理サービスの流れ</h4>
			</div>
			<div class="container mb50">
				<div class="row mb50">
					<div class="col-sm-6" data-aos="fade-right">
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/service_manage_01.jpg" alt="お預かり">
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<p class="fontEn h4 titleLine mb10">Step 01</p>
						<div>
							<h4 class="h4 mb30 titleBd inlineBlock">お預かり</h4>
						</div>
						<p>貴社が加入している保険証券を一式、お預かりします。</p>
					</div>
				</div>
				<div class="row mb50">
					<div class="col-sm-6" data-aos="fade-right">
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/service_manage_02.jpg" alt="棚卸・診断">
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<p class="fontEn h4 titleLine mb10">Step 02</p>
						<div>
							<h4 class="h4 mb30 titleBd inlineBlock">棚卸・診断</h4>
						</div>
						<p>当社独自のノウハウで、加入保険の棚卸・簡易診断を実施します。</p>
					</div>
				</div>
				<div class="row mb50">
					<div class="col-sm-6" data-aos="fade-right">
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/service_manage_03.jpg" alt="アドバイス">
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<p class="fontEn h4 titleLine mb10">Step 03</p>
						<div>
							<h4 class="h4 mb30 titleBd inlineBlock">アドバイス</h4>
						</div>
						<p>診断結果を簡易レポートにまとめ報告。同時に問題点などをアドバイスします。</p>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6" data-aos="fade-right">
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/service_manage_04.jpg" alt="保健管理">
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<p class="fontEn h4 titleLine mb10">Step 04</p>
						<div>
							<h4 class="h4 mb30 titleBd inlineBlock">保健管理</h4>
						</div>
						<p>全ての既契約（保険会社を問わず）を一括して満期管理・保険資産管理をいたします。</p>
						<p>業務負担を軽減させながら、最適なタイミングで最適なアドバイスを実施、経営をサポートします。</p>
						<p class="bold">・保険管理代行<br>・満期管理、保険資産管理<br>・法人ライフプランニング<br>・経営全般のアドバイス</p>
					</div>
				</div>
			</div>
			<!--
			<div class="container">
				<div class="row mb30" data-aos="fade-up">
					<div class="col-sm-4">
						<div class="topAboutBox">
							<img class="pageServiceIcoImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_service_ico_01.png" alt="">
							<h4 class="h4 mb10 titleBd text-center">自動車保険</h4>
							<p class="gray text_m">火災や風水害などの自然災害から日常生活の思いもよらないリスクまで、大切な家（建物）や財産（家財道具など）を守ります。</p>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="topAboutBox">
							<img class="pageServiceIcoImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_service_ico_02.png" alt="">
							<h4 class="h4 mb10 titleBd text-center">自動車保険</h4>
							<p class="gray text_m">火災や風水害などの自然災害から日常生活の思いもよらないリスクまで、大切な家（建物）や財産（家財道具など）を守ります。</p>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="topAboutBox">
							<img class="pageServiceIcoImg mb10" src="<?php echo get_template_directory_uri();?>/img/page_service_ico_03.png" alt="">
							<h4 class="h4 mb10 titleBd text-center">自動車保険</h4>
							<p class="gray text_m">火災や風水害などの自然災害から日常生活の思いもよらないリスクまで、大切な家（建物）や財産（家財道具など）を守ります。</p>
						</div>
					</div>
				</div>
			</div>
			-->
			<div class="width720">
				<div class="text-center">
					<h4 class="h3 mb10 titleBd inlineBlock mb50">STEP1〜3までは<span class="bold">無料</span>です。<br><span class="bold">適切な経営判断</span>をおこなうためにも、<br>まずはお気軽に<span class="bold">現状把握</span>に取り組んでみませんか？</h4>
				</div>
			</div>
		</div>
	</div>
</section>





<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>