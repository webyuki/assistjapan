<?php get_header(); ?>
<main>

<section class="pageHeader bgImg margin bgMainColor" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_goki.png')">
	<div class="container">
		<div class="white">
			<h2 class="bold h3">ゴキブリ・ネズミ駆除・予防</h2>
			<h3 class="titleHeader mincho subColor">ゴキブリ</h3>
			<div class="row">
				<div class="col-sm-6">
					<p class="text_m white">衛生面での注意が必要な店舗様を中心に、月に1度の定期点検と駆除・予防を行っています。侵入経路の遮断と安全な薬剤の散布により、年間を通じて清潔な環境を守ります。</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="margin pageGreeting">
	<div class="container">
		<h3 class="bold h3 text-center mb10">定期的な予防でお客さまからの信頼を守ります</h3>
		<div class="titleBd mb10"></div>
		<p class="fontEn h5 bold mainColor text-center mb30">Protect</p>
		<div class="row mb30">
			<div class="col-sm-6">
				<h4 class="h3 bold mainColor mb10">店舗様を中心に、月に1度の定期点検と予防を行っています</h4>
				<p>ヒカリ消毒では飲食店などの店舗様を中心に、月に1度の定期点検と予防を行っています。点検ではゴキブリやネズミが発生していないかを確認したのち、侵入経路になる可能性のある穴をふさぎ、予防のための薬剤を散布します。薬剤は人体にほぼ影響ないものを使用し、食べ物や人が触れない箇所にのみ散布しています。</p>
			</div>
			<div class="col-sm-6">
				<img class="" src="<?php echo get_template_directory_uri();?>/img/page_goki_01.jpg" alt="">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-push-6">
				<h4 class="h3 bold mainColor mb10">食中毒の原因となるゴキブリやネズミは、発生させないことが重要です</h4>
				<p>ゴキブリやネズミは夜間活動することが多く、発生しても建物の中が明るい時間帯は気づかないことがあります。お客さまが触れる場所や食べ物を扱う場所に触れると、食中毒の原因となる可能性も。プロの目による定期的な点検を受け、建物の構造に合った予防を行うことで発生しない環境を作ることが大切です。</p>
			</div>
			<div class="col-sm-6 col-sm-pull-6">
				<img class="" src="<?php echo get_template_directory_uri();?>/img/page_goki_02.jpg" alt="">
			</div>
		</div>
	</div>
</section>

	
<section class="margin bgGreen pageCommonRecommend">
	<div class="container">
		<h3 class="bold h3 text-center mb10">このような方に<br>「ゴキブリ・ネズミ駆除・予防」をオススメします</h3>
		<div class="titleBd mb10"></div>
		<p class="fontEn h5 bold mainColor text-center mb30">Recommend</p>
		
		<div class="pageCommonRecommendBox">
			<ul class="inline_block h4 bold">
				<li><i class="fa fa-check-circle"></i>飲食店など、店舗を経営している</li>
				<li><i class="fa fa-check-circle"></i>家や建物の中でゴキブリやネズミを発見した</li>
				<li><i class="fa fa-check-circle"></i>定期的にプロのチェックを依頼したい</li>
			</ul>
		</div>		
		
	</div>
</section>

<section class="topWorks margin">
	<div class="container">
		<h3 class="bold h3 mb10">実績紹介</h3>
		<div class="titleBd mb10 titleBdLeft"></div>
		<p class="fontEn h5 bold mainColor mb30">Works</p>
		<div class="row mb30">
		
        	<?php
				$args = array(
					'post_type' => 'works', //投稿タイプ名
					'posts_per_page' => 3, //出力する記事の数
					'tax_query' => array(
						array(
							'taxonomy' => 'works_cate', //タクソノミー名
							'field' => 'slug',
							//'terms' => $term_slug //タームのスラッグ
							'terms' => array( 'goki','nezumi' ) //タームのスラッグ
						)
					)
				);
			?>
			<?php
				$myposts = get_posts( $args );
				foreach ( $myposts as $post ) : setup_postdata( $post );
			?>

			<?php get_template_part('content-post-works'); ?>

            <?php endforeach; ?>
		
		</div>
		<a href="<?php echo home_url();?>/works_cate/goki/" class="button white tra text-center">詳しく見る</a>
	</div>
	
	
</section>


<section class="pageCommonFee margin bgGreen">
	<div class="container">
		<h3 class="bold h3 text-center mb10">ゴキブリ・ネズミ駆除の料金</h3>
		<div class="titleBd mb10"></div>
		<p class="fontEn h5 bold mainColor text-center mb30">Prices</p>
		<div class="row">
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				<div class="pageCommonFeeBox mb30">
					<h5 class="mainColor h4 bold">ゴキブリ・ネズミ駆除</h5>
					<p class="pageFeeColor">月／<span class="h2 bold">5,000</span>円～</p>
					<p class="grayColor text_m pageCommonFeeBoxBorder">年間管理・飲食店向けになります。</p>
					<ul class="text_m">
						<li><i class="fa fa-check mainColor"></i>調査・診断・見積もり無料</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_template_part('parts/temp-flow'); ?>

<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>