<?php get_header(); ?>

<main>

<section class="relative topStaff">
	<div class="container">
		<div class="bgImg pageFvImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_about_fv.png');" data-aos="fade-right"></div>
	<div class="pageFvBoxWrap topFvBoxWrap text-center" data-aos="fade-left">
		<div class="topFvBoxText">
			<div class="text-center">
				<p class="fontEn h3 titleLine mb10">About Us</p>
				<h2 class="h3 mb10">菊池保険サービスとは？</h2>
			</div>
		</div>
	</div>
	</div>
</section>

<section class="pageAboutGreeting margin">
	<div class="container">
		<div class="row">
			<div class="col-sm-6" data-aos="fade-right">
				<p class="fontEn h3 titleLine mb10">Greeting</p>
				<h3 class="h3 bold mb30 mainColor">代表あいさつ</h3>

<p>2018年夏、西日本を襲った豪雨のあと私が真備の自宅に戻れたのは、雨がやんだ3日後のことでした。すでに水は引いていたものの、自宅は2階まで浸水。住める状態でないことは一目瞭然でした。</p>
<p>思うように進まない復旧作業のなかで直面したのは、自宅にはこの災害による被害を十分にカバーできるだけの保険をかけていなかったという事実。保険業を営んでいながら、甘かったとしか言いようのない自らの認識を責めました。</p>
<p class="mb30">けれど私たちは、いつまでも後悔の中に立ち止まっているわけにはいきません。</p>
<p>取り組むべきはこの教訓をいかし、一人でも多くのお客さまと、一社でも多くの企業さまのリスクを減らしていくこと。この地でお客さまとのつながりを築いてきた社員とともに、まちの復興と、ここに住むお客さまの生活を支えてまいります。</p>
<p>これまでは仕事として取り組んできたサービスのさらに奥深くへ踏み込み、ライフワークとして地域の発展に貢献していく。</p>
<p class="mb30">新たな決意とともに、私たちはより地域とお客さまのニーズにお応えできるサービスをご提供してまいります。</p>


				<p class="text_m mb0">代表取締役</p>
				<p class="h3">菊池 達也</p>
			</div>
			<div class="col-sm-6" data-aos="fade-left">
				<img class="" src="<?php echo get_template_directory_uri();?>/img/page_about_01.jpg" alt="代表取締役 菊池 達也">
			</div>
		</div>
	</div>
</section>


<section class="margin">
	<div class="container">
		<div class="padding paddingInner bgWhite">
			<div class="text-center">
				<p class="fontEn h3 titleLine mb10">Concept</p>
				<h3 class="h3 bold mb30 mainColor">菊池保険のこだわり</h3>
			</div>
			<div class="width720 mb50">

<p>私たちは自分が「楽しい、うれしい」と感じる気持ちを、どうすれば周りの人たちの喜びにつなげていけるかを、日々考えて行動していきたいと思っています。</p>
<p>お客さまと重ねる時間が、いつか誰かの大切なものを守る力になる。目には見えないサービスであっても、私たちは必ず来る未来のために、安心の絆を固く結んでいきます。</p>

			</div>
			<div class="container">
				<div class="row mb50">
					<div class="col-sm-6" data-aos="fade-right">
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/top_service_01.jpg" alt="菊池保険のこだわり">
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<h4 class="h3 mb30 titleBd">お客さまに喜んでいただけること。</h4>
						<p class="">私たちがご提供するサービスは、まずお客さまご自身やご家族、社員の皆さま、そしてその財産を守ることを最優先しています。安心して日々を過ごせる支えを整えたのち、資産運用や貯蓄などのプランをご案内。
見直したムダを必要な保障のある商品に変え、豊かな生活へとつなげていく。人生のどのステージにおいても、お客さまが安心して暮らせるサービスをご提供してまいります。
</p>
					</div>
				</div>
				<div class="row mb50">
					<div class="col-sm-6" data-aos="fade-right">
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/top_service_02.jpg" alt="人の役に立った実感を持てる人になること">
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<h4 class="h3 mb30 titleBd">人の役に立った実感を持てる人になること。</h4>
						<p class="">保険はお客さまにとって、非常にわかりにくい商品です。個人・法人を問わず、多くの方がご自身の契約している保険の内容を把握できていないと感じ、もしもの時に不安を抱えていらっしゃいます。
私たちは商品をできる限りわかりやすく、「いつ、何ができるのか」をご説明し、もしもの時にこそ役立つサービスの提供によって、仕事へのやりがいを追究してまいります。
</p>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6" data-aos="fade-right">
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/top_service_03.jpg" alt="社員が心豊かな暮らしをすること。">
					</div>
					<div class="col-sm-6" data-aos="fade-left">
						<h4 class="h3 mb30 titleBd">社員が心豊かな暮らしをすること。</h4>
						<p class="">保険を扱う仕事にたずさわるなかで、お客さまの安心や喜びは何よりの励みです。自らが身につけた知識でお客さまの大切なものを守れたという実感は、この仕事ならでは。
そのためには知識の蓄積やお客さまとのコミュニケーションなど、日々の積み重ねを大切に、社員一同心豊かな毎日を心がけてまいります。</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="margin">
	<div class="container">
		<div class="padding paddingInner bgWhite">
			<div class="text-center">
				<p class="fontEn h3 titleLine mb10">Company</p>
				<h3 class="h3 bold mb30 mainColor">会社概要</h3>
			</div>
			<!--
			<div class="width720">
				<img class="mb50" src="<?php echo get_template_directory_uri();?>/img/page_about_02.jpg" alt="">
			</div>
			-->
			<div class="pageAboutCompanyUl width720 mb50" data-aos="fade-up">
				<ul>
					<li>会社名</li>
					<li>株式会社菊池保険サービス</li>
				</ul>
				<ul>
					<li>所在地</li>
					<li>〒7101306 岡山県倉敷市真備町有井69-1</li>
				</ul>
				<ul>
					<li>連絡先</li>
					<li>TEL：086-697-5757　FAX：086-697-0203</li>
				</ul>
				<ul>
					<li>メールアドレス</li>
					<li><a href="mailto:info@kikuchi-hoken.com">info@kikuchi-hoken.com</a></li>
				</ul>
				<ul>
					<li>営業時間</li>
					<li>9:00 〜 18:00</li>
				</ul>
				<ul>
					<li>定休日</li>
					<li>土曜・日曜・祝日 ※緊急時は24時間365日受付</li>
				</ul>
				<ul>
					<li>代表取締役</li>
					<li>菊池 達也</li>
				</ul>
				<ul>
					<li>事業内容</li>
					<li>生命保険の募集に関する業務、損害保険代理業</li>
				</ul>
				<ul>
					<li>取扱保険</li>
					<li>

[損害保険]<br>
あいおいニッセイ同和損害保険株式会社<br><br>

[生命保険]<br>
日本生命保険相互会社<br>
三井住友海上あいおい生命保険株式会社<br>
オリックス生命保険株式会社<br>
メットライフ生命保険株式会社<br>
エヌエヌ生命保険株式会社<br>
ソニー生命保険株式会社
					
					</li>
				</ul>
			</div>
			<div class="width720">
<iframe class="company_map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13130.295230993543!2d133.69839975548632!3d34.64020900344258!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x251887a1fb7fa720!2z6I-K5rGg5L-d6Zm644K144O844OT44K5!5e0!3m2!1sja!2sjp!4v1548309430724" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</section>




<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>