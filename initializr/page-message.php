<?php get_header(); ?>

<main>
<section class="pageHeader relative">
	<div class="bgGrad pageHeaderText relative" data-aos="fade-right">
		<p class="pageHeaderEn fontEnBrush white">Message</p>
		<h3 class="h2 bold white">代表メッセージ</h3>
	</div>
	<div class="pageHeaderImgBox bgImg absolute" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_message_fv.jpg')" data-aos="fade-left"></div>
	
</section>

<div class="pageMessageMes mb100">
	<div class="container">
		<p class="fontEn h1 mainColor text-center">Entertain Your Carieer</p>
		<h3 class="h3 bold subColor mb50 text-center">どんな自分になりたいですか？</h3>
		<div class="w600">
			<div class="shadowBox relative mb80" data-aos="fade-up">
				<img class="" src="<?php echo get_template_directory_uri();?>/img/page_message_01.jpg" alt="">
				<div class="shadowBoxShadow absolute bgGrad"></div>
			</div>
			<div class="text-center" data-aos="fade-up">

<p>人生の3分の1の時間は「仕事」です。</p>
<p>毎日をイキイキ過ごすために、人生をより豊かにするために、<Br>どんな「仕事」をするのかは、とても大切です。</p>

<p class="mb30">クレストは「新卒至上主義」の日本において、<br>
既卒・第二新卒と呼ばれる若者たちが<br>
自身のキャリアをより魅力的にするための<br>
最高の「仕事」を提供するために存在する建設会社です。</p>

<p>人生の3分の1の時間は「仕事」です。</p>
<p>毎日をイキイキ過ごすために、人生をより豊かにするために、<Br>どんな「仕事」をするのかは、とても大切です。</p>

<p class="mb50">クレストは「新卒至上主義」の日本において、<br>
既卒・第二新卒と呼ばれる若者たちが<br>
自身のキャリアをより魅力的にするための<br>
最高の「仕事」を提供するために存在する建設会社です。</p>

<p class="mincho mb0">代表取締役</p>
<p class="mincho h2">松本健司</p>

			</div>
		</div>
	</div>
</div>





<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>