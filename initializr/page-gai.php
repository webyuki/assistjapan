<?php get_header(); ?>
<main>

<section class="pageHeader bgImg margin bgMainColor" style="background-image:url('<?php echo get_template_directory_uri();?>/img/fv_gai.png')">
	<div class="container">
		<div class="white">
			<h2 class="bold h3">害虫・害獣　駆除</h2>
			<h3 class="titleHeader mincho subColor">害虫・害獣</h3>
			<div class="row">
				<div class="col-sm-6">
					<p class="text_m white">ダニ・ノミなどの害虫や、イタチなどの害獣（益獣）による被害は、アレルギーや呼吸器系疾患など、健康にも悪い影響を及ぼします。根本的な駆除は知識と経験豊かなプロにお任せください。</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="margin pageGreeting">
	<div class="container">
		<h3 class="bold h3 text-center mb10">畳の裏まで徹底的に。プロによる効果的な駆除</h3>
		<div class="titleBd mb10"></div>
		<p class="fontEn h5 bold mainColor text-center mb30">Protect</p>
		<div class="row mb30">
			<div class="col-sm-6">
				<h4 class="h3 bold mainColor mb10">薬剤の噴霧だけでは十分ではありません</h4>
				<p>ダニ・ノミをはじめとする害虫は、畳の中や裏など、普段動かすことの少ない場所に発生します。市販されている噴霧タイプやスプレータイプの薬剤を散布するだけでは、一時的に表面の害虫を追い払うことしかできません。ヒカリ消毒では畳の裏まで薬剤の効果が届くよう、畳をすべて上げ、床下まで駆除を行います。害虫の生態に合わせ、効率的な作業を行っています。</p>
			</div>
			<div class="col-sm-6">
				<img class="" src="<?php echo get_template_directory_uri();?>/img/page_gai_01.jpg" alt="">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-push-6">
				<h4 class="h3 bold mainColor mb10">害獣は郊外だけでなく、市街地でも発生しています</h4>
				<p>被害が報告されている害獣の多くは、郊外だけでなく、市街地など比較的自然の少ない場所でも発生しています。また益獣であるイタチは、駆除ではなく保護して山などに逃がさなければならないため、侵入経路や生活場所など、適切な位置を見きわめて罠を仕掛けることが必要です。床下や天井裏など、狭い場所での駆除や捕獲はプロにご依頼ください。</p>
			</div>
			<div class="col-sm-6 col-sm-pull-6">
				<img class="" src="<?php echo get_template_directory_uri();?>/img/page_gai_02.jpg" alt="">
			</div>
		</div>
	</div>
</section>

	
<section class="margin bgGreen pageCommonRecommend">
	<div class="container">
		<h3 class="bold h3 text-center mb10">このような方に<br>「害虫・害獣駆除」をオススメします</h3>
		<div class="titleBd mb10"></div>
		<p class="fontEn h5 bold mainColor text-center mb30">Recommend</p>
		
		<div class="pageCommonRecommendBox">
			<ul class="inline_block h4 bold">
				<li><i class="fa fa-check-circle"></i>体にかゆみや痛みを感じた</li>
				<li><i class="fa fa-check-circle"></i>アレルギーや呼吸器系疾患のあるご家族がいる</li>
				<li><i class="fa fa-check-circle"></i>床下や天井裏で物音がする</li>
			</ul>
		</div>		
		
	</div>
</section>

<section class="topWorks margin">
	<div class="container">
		<h3 class="bold h3 mb10">実績紹介</h3>
		<div class="titleBd mb10 titleBdLeft"></div>
		<p class="fontEn h5 bold mainColor mb30">Works</p>
		<div class="row mb30">

        	<?php
				$args = array(
					'post_type' => 'works', //投稿タイプ名
					'posts_per_page' => 3, //出力する記事の数
					'tax_query' => array(
						array(
							'taxonomy' => 'works_cate', //タクソノミー名
							'field' => 'slug',
							//'terms' => $term_slug //タームのスラッグ
							'terms' => 'gai' //タームのスラッグ
						)
					)
				);
			?>
			<?php
				$myposts = get_posts( $args );
				foreach ( $myposts as $post ) : setup_postdata( $post );
			?>

			<?php get_template_part('content-post-works'); ?>

            <?php endforeach; ?>

		</div>
		<a href="<?php echo home_url();?>/works_cate/gai/" class="button white tra text-center">詳しく見る</a>
	</div>
	
	
</section>


<section class="pageCommonFee margin bgGreen">
	<div class="container">
		<h3 class="bold h3 text-center mb10">害虫・害獣駆除の料金</h3>
		<div class="titleBd mb10"></div>
		<p class="fontEn h5 bold mainColor text-center mb30">Prices</p>
		<div class="row">
			<div class="col-sm-4"></div>
			<div class="col-sm-4">
				<div class="pageCommonFeeBox mb30">
					<h5 class="mainColor h4 bold">害虫・害獣駆除</h5>
					<p class="pageFeeColor"><span class="h2 bold">お問い合わせください</span></p>
					<p class="grayColor text_m pageCommonFeeBoxBorder">種類によりますので、まずお問い合わせください。</p>
					<ul class="text_m">
						<li><i class="fa fa-check mainColor"></i>調査・診断・見積もり無料</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>

<?php get_template_part('parts/temp-flow'); ?>

<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>