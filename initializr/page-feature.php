<?php get_header(); ?>
<main>


<section class="pageHeader bgMainColor relative padding">
	<div class="bgTextBox absolute">
		<p class="bgText fontEn white">Our Feature</p>
	</div>
	<div class="container">
		<div class="text-center white">
			<p class="fontEn h0 fontEnNegaMb">Feature</p>
			<h3 class="h3 bold mb50 titleBd titleBdWhite">当社の特徴</h3>
		</div>
	</div>
</section>

<section class="pageFeatureMember margin" id="manage">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-7" data-aos="fade-right">
				<div class="bgImg pageFeatureBg mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_feature_member_01.jpg')"></div>
			</div>
			<div class="col-sm-5" data-aos="fade-left">
				<div class="pageFeatureMemTextRight">
					<div class="mainColor">
						<p class="fontEn h0 fontEnNegaMb">Management</p>
						<h3 class="h3 bold mb30 titleBd">堅実経営</h3>
					</div>
					<p>平成23年に会社設立して以来、<span class="bold">「安心・安全・誠実なサービスを提供する」</span>という理念のもと経営してきました。おかげさまで売上高・社員数・保有車両、全て安定成長しています。</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="topRecruit3col margin relative">
	<div class="bgTextBox bgTextBox90 absolute">
		<p class="bgText fontEn mainColor">Management</p>
	</div>
	<div class="container">
		<div class="row" data-aos="fade-up">
			<div class="col-sm-4">
				<div class="topRecruit3colBox shadow relative mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/feature_img_02.jpg" alt="">
					<div class="topRecruit3colText relative">
						<h4 class="mainColor h3 bold mb10 text-center">売上高</h4>
						<p class="text_m">おかげさまで毎年（少しずつですが）増収しています。</p>
						<div class="bgTextBox bgTextBoxS absolute">
							<p class="bgText bgTextS fontEn mainColor">01</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="topRecruit3colBox shadow relative mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/top_recruit_03.jpg" alt="">
					<div class="topRecruit3colText relative">
						<h4 class="mainColor h3 bold mb10 text-center">社員数</h4>
						<p class="text_m">最初は2名に始まった心和ですが、現在は10名まで仲間が増えました。</p>
						<div class="bgTextBox bgTextBoxS absolute">
							<p class="bgText bgTextS fontEn mainColor">02</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="topRecruit3colBox shadow relative mb30">
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/feature_img_01.jpg" alt="">
					<div class="topRecruit3colText relative">
						<h4 class="mainColor h3 bold mb10 text-center">保有車両</h4>
						<p class="text_m">平ボディ、ウイング、冷蔵、冷凍車など現在16台の車両を保有しています。</p>
						<div class="bgTextBox bgTextBoxS absolute">
							<p class="bgText bgTextS fontEn mainColor">03</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="" id="family">
	<div class="flex flexPc">
		<div class="topRecruitBox bgMainColor flexS40" data-aos="fade-right">
			<div class="white titleBd titleBdWhite mb30">
				<p class="fontEn h0 fontEnNegaMb">Members</p>
				<h3 class="h3 bold">社員は家族</h3>
			</div>
			<img class="mb10 sp" src="<?php echo get_template_directory_uri();?>/img/_page_feature_member_01.jpg" alt="">
			
			<div class="white mb50">
				<p>心和では社員を家族だと考えいます。社内も非常に風通しが良く、笑いも多い職場です。
業務中は一人になる仕事だからこそ、お互いを想い合える関係性を大切にしています。</p>
			</div>
		</div>
		<div class="bgImg padding flexS60 pc" style="background-image:url('<?php echo get_template_directory_uri();?>/img/_page_feature_member_01.jpg')" data-aos="fade-left"></div>
	</div>
</section>

<section class="pageFeaVeSp padding relative">
	<div class="bgTextBox bgTextBox90 absolute">
		<p class="bgText fontEn mainColor">Members</p>
	</div>
	<div class="container width980">
		<div class="flex">
			<div class="bgImg flex flexSplit pc" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_feature_vihicle02.jpg')" data-aos="fade-right"></div>
			<div class="flexSplit bgSubColor topRecruitBox" data-aos="fade-left">
				<div class="mainColor mb30">
					<h4 class="h2 bold">安心・安全な仕事をして欲しい</h3>
				</div>
				<img class="mb10 sp" src="<?php echo get_template_directory_uri();?>/img/page_feature_vihicle02.jpg" alt="">
				<div class="mb50">
					<p>お客様に安心・安全なサービスを提供するのと同様、家族である社員一人一人にも安心・安全な仕事に取り組める環境を提供できるよう努めています。</p>
					<p>その結果、お客様にも確かなサービスを提供でき、お客様も社員も会社もwin-win-winになれると信じています。</p>
				</div>
			</div>
		</div>
		<!--
		<div class="flex">
			<div class="flexSplit bgMainColor topRecruitBox" data-aos="fade-right">
				<div class="white mb30">
					<h4 class="h2 bold">全国ネットを活かして柔軟に対応</h3>
				</div>
				<img class="mb10 sp" src="<?php echo get_template_directory_uri();?>/img/page_feature_vihicle02.jpg" alt="">
				<div class="mb50 white">
					<p>当社の創業は1947（昭和22）年。まだ廃棄物処理への関心が薄かった時代から、電話機や交換機などの引取・解体業務を手掛けてきました。

この基本方針は、ダイヤル式の電話機を見かけなくなった現代でも変わりません。機器類の「リサイクル業務」を大黒柱に据えつつ、中古パソコンの販売による「リユース業務」と電子機器類から資源を回収する「リデュース業務」を加えた三本柱によって、地球の環境問題に貢献して参りました。</p>
				</div>
			</div>
			<div class="bgImg flex flexSplit pc" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_feature_vihicle02.jpg')" data-aos="fade-left"></div>
		</div>
		<div class="flex">
			<div class="bgImg flex flexSplit pc" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_feature_vihicle02.jpg')" data-aos="fade-right"></div>
			<div class="flexSplit bgSubColor topRecruitBox" data-aos="fade-left">
				<div class="mainColor mb30">
					<h4 class="h2 bold">全国ネットを活かして柔軟に対応</h3>
				</div>
				<img class="mb10 sp" src="<?php echo get_template_directory_uri();?>/img/page_feature_vihicle02.jpg" alt="">
				<div class="mb50">
					<p>当社の創業は1947（昭和22）年。まだ廃棄物処理への関心が薄かった時代から、電話機や交換機などの引取・解体業務を手掛けてきました。

この基本方針は、ダイヤル式の電話機を見かけなくなった現代でも変わりません。機器類の「リサイクル業務」を大黒柱に据えつつ、中古パソコンの販売による「リユース業務」と電子機器類から資源を回収する「リデュース業務」を加えた三本柱によって、地球の環境問題に貢献して参りました。</p>
				</div>
			</div>
		</div>
		-->
	</div>
</section>

<section class="" id="safety">
	<div class="flex flexPc">
		<div class="bgImg padding flexS60 pc" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_feature_vihicle03.jpg')" data-aos="fade-right"></div>
		<div class="topRecruitBox bgMainColor flexS40" data-aos="fade-left">
			<div class="white titleBd titleBdWhite mb30">
				<p class="fontEn h0 fontEnNegaMb">Safety</p>
				<h3 class="h3 bold">安心・安全のサービス</h3>
			</div>
			<img class="mb10 sp" src="<?php echo get_template_directory_uri();?>/img/page_feature_vihicle03.jpg" alt="">
			
			<div class="white mb50">
				<p>社員の働き方改革、安全教育などの取り組みにより、お客様に安心・安全なサービスの提供に努めています。地元岡山に根差し、確かな運送を実現しています。</p>
			</div>
		</div>
	</div>
</section>
<section class="topService padding relative">
	<div class="bgTextBox bgTextBox90 absolute">
		<p class="bgText fontEn mainColor">Safety</p>
	</div>
	<div class="container">
		<div class="row mb50" data-aos="fade-up">
			<div class="col-sm-4">
				<div class="topServiceBox shadow relative mb30 matchHeight">
					<img class="topServiceIco mb10" src="<?php echo get_template_directory_uri();?>/img/icon_work_easty.png" alt="">
					<h4 class="mainColor h4 bold mb10 text-center">働きやすさの追求</h4>
					<p class="text_m">社員同士のコミュニケーション、安全教育、長時間労働の削減などにより社員一人一人の働きやすさを追求しています。</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="topServiceBox shadow relative mb30 matchHeight">
					<img class="topServiceIco mb10" src="<?php echo get_template_directory_uri();?>/img/icon_peach.png" alt="">
					<h4 class="mainColor h4 bold mb10 text-center">地元岡山に密着</h4>
					<p class="text_m">地元岡山に密着し、近・中距離運送を中心に活動しています。無理な長距離は対応せず、地域のお客様に確実で丁寧なサービスを提供しています。</p>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="topServiceBox shadow relative mb30 matchHeight">
					<img class="topServiceIco mb10" src="<?php echo get_template_directory_uri();?>/img/icon_accident.png" alt="">
					<h4 class="mainColor h4 bold mb10 text-center">事故率の低さ</h4>
					<p class="text_m">様々な取り組みにより開業以来、大きな事故はほとんどありません。</p>
				</div>
			</div>
		</div>
	</div>
</section>



<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>