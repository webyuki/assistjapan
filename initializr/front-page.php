<?php get_header(); ?>
<main>


<section class="relative topFvSection2">
	<div class="topFv2">
		<div class="main_imgBox2">
			<div class="main_img2" style="background-image:url('<?php echo get_template_directory_uri();?>/img/cp_fv_01.jpg');"></div>
			<div class="main_img2" style="background-image:url('<?php echo get_template_directory_uri();?>/img/cp_fv_02.jpg');"></div>
			<div class="main_img2" style="background-image:url('<?php echo get_template_directory_uri();?>/img/cp_fv_03.jpg');"></div>
		</div>
	</div>
	<div class="topFvBoxText2 white absolute text-center">
        <img class="mb30 fvLogo" src="<?php echo get_template_directory_uri();?>/img/logo.png" alt="企業の未来を共に創造する">
		<h2 class="h00 bold mb30">企業の未来を<br class="sp">共に創造する</h2>
		<!--<p class="fontEn h3">Supporting Life!</p>-->
	</div>
</section>



<section class="topGreeding margin" id="greeting">
	<div class="container">
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">メッセージ</h3>
				<p class="mainColor fontEn h4">Message</p>
			</div>
		</div>
		<div class="width720">

<!--
<div class="mb30 text-center">
	<h4 class="bold titleLine">お客様と車に誠実に向き合い、<br>プロのサービスを提供したい</h4>
</div>		
-->
            
<p>アシスト・ジャパン協同組合のホームページをご覧いただき、ありがとうございます。</p>
<!--<p>当組合は、地元玉野市の人材不足解消の寄与を目的に2019年に設立されました。</p>-->
<p>技能実習生は若く真面目で向上心があるため、若年層の定着率が低い今、技能習得に熱心な技能実習生を継続的に受入れることで、高齢化していく職場に於いても生産性向上や職場の活性化につながります。</p>
<p>また、技能実習生を受入れることは、技能実習生の母国の産業界発展に貢献すると同時に、社内の国際交流により国際理解を深める良い機会にもなります。</p>
<!--<p>私どもの特徴は、玉野市に会社があることと、組合職員が全員玉野市民であることです。</p>-->
<p>実習生の受け入れ企業様や実習生自身が持つ心配や不安を、早急に駆けつけ対応ができ、電話や、メールではなく直接的にアプローチできることは、必ずお役に立てることと考えています。</p>
<p class="mb30">ぜひ、同制度のご活用をご検討ください。</p>


                <img class="mb20" src="<?php echo get_template_directory_uri();?>/img/top_message_01.jpg" alt="">


			<div class="text-center">
				<p class="text_m mb0 bold">代表理事</p>
				<p class="bold h3">岡崎 晋典</p>
			</div>
		</div>
		
	</div>
</section>

 
<section class="topCompany margin" id="company">
	<div class="container">
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">組合概要</h3>
				<p class="mainColor fontEn h4">Company</p>
			</div>
		</div>
		<div class="row">
            <div class="col-sm-2"></div>
			<div class="col-sm-8" data-aos="fade-up">
				<div class="topCompanyDl">
					<dl>
						<dt>名称</dt>
						<dd>アシスト・ジャパン協同組合</dd>
						<dt>設立</dt>
						<dd>2019年7月24日</dd>
						<dt>所在地</dt>
						<dd>〒706-0001<br>岡山県玉野市田井3丁目5681番地12</dd>
						<dt>電話</dt>
						<dd>0863-33-1928</dd>
						<dt>FAX</dt>
						<dd>0863-33-1926</dd>
						<dt>代表理事</dt>
						<dd>岡崎 晋典</dd>
						<dt>E-mail</dt>
						<dd>info@assist-japan.biz</dd>
						<dt>所属団体</dt>
						<dd>岡山県中小企業団体中央会</dd>
						<dt>許可種別</dt>
                        <dd>特定監理事業</dd>
						<dt>アライアンスパートナー</dt>
                        <dd>
【送り出し機関】<br>
・<a class="linkA tra mainColor" href="http://eki.com.vn/" target="_blank">ＥＫ栄進アライアンス</a> (所在地：ハノイ)<br>
・<a class="linkA tra mainColor" href="https://minhthanhgroup.net/ja/" target="_blank">ミンタングループ</a> (所在地：ハノイ)<br>
他数社<br>
※受け入れ企業様のご希望に対応いたします。<br><br>

【一般社団法人】<br>
提携協会<br>
（一社）留学生支援・雇用促進協会（ＡＳＩＳ）
                        </dd>
                        
					</dl>
				</div>
			</div>
		</div>
	</div>
</section>  

<section class="topService padding" id="service">
	<div class="container">
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">事業内容</h3>
				<p class="mainColor fontEn h4">Service</p>
			</div>
		</div>
        
		<div class="row" data-aos="fade-up">
			<div class="col-sm-4">
				<div class="topServiceBox mb30">
					<div class="topServiceBoxImgBox">
						<div class="topServiceBoxImg bgImg mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_feature_01.jpg');"></div>
					</div>
					<h4 class="bold h4 text-center mb10 mainColor">共同購買事業</h4>
                </div>
			</div>
			<div class="col-sm-4">
				<div class="topServiceBox mb30">
					<div class="topServiceBoxImgBox">
						<div class="topServiceBoxImg bgImg mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_feature_02.jpg');"></div>
					</div>
					<h4 class="bold h4 text-center mb10 mainColor">宣伝支援事業</h4>
                </div>
			</div>
			<div class="col-sm-4">
				<div class="topServiceBox mb30">
					<div class="topServiceBoxImgBox">
						<div class="topServiceBoxImg bgImg mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_feature_03.jpg');"></div>
					</div>
					<h4 class="bold h4 text-center mb10 mainColor">外国人技能実習生共同受入事業</h4>
                </div>
			</div>
		</div>
        
		<div class="row mb50" data-aos="fade-up">
			<div class="col-sm-2">
            </div>
			<div class="col-sm-4">
				<div class="topServiceBox mb30">
					<div class="topServiceBoxImgBox">
						<div class="topServiceBoxImg bgImg mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_feature_04.jpg');"></div>
					</div>
					<h4 class="bold h4 text-center mb10 mainColor">経営及び技術の改善向上又は、知識の普及を図るための教育及び情報の提供事業</h4>
                </div>
			</div>
			<div class="col-sm-4">
				<div class="topServiceBox mb30">
					<div class="topServiceBoxImgBox">
						<div class="topServiceBoxImg bgImg mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_feature_05.jpg');"></div>
					</div>
					<h4 class="bold h4 text-center mb10 mainColor">福利厚生に関する事業</h4>
                </div>
			</div>
		</div>
		<div class="text-center mb30">
			<div class="inlineBlock">
				<h3 class="h4 bold titleBd mb10">その他事業内容</h3>
			</div>
		</div>
        <div class="width720 mainColor bold text-center h4 mb50">
            <ul>
                <li>・ 留学生人材紹介</li>
                <li>・ エンジニア紹介</li>
                <li>・留学生アルバイト紹介</li>
            </ul>
        </div>
        <p class="text-center mb30">詳細は事業案内パンフレットをダウンロードしてください。</p>
        <div class="">
            <a href="<?php echo get_template_directory_uri();?>/file/document.pdf" target="_blank" class="button buttonBgWhite bold mb10 white tra text-center mainColor">ダウンロードする</a>
        </div>
        
	</div>
</section>

<!--
<section class="topTrouble padding bgSubColor">
	<div class="container">
	
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">こんなお悩みはありませんか？</h3>
				<p class="mainColor fontEn h4">Trouble</p>
			</div>
		</div>
		<div class="pageCommonRecommendBox width720" data-aos="fade-up">
			<ul class="inline_block h5">
				<li><i class="fa fa-check-circle"></i><span>名刺やチラシ、パンフレットを制作したい</span></li>
				<li><i class="fa fa-check-circle"></i><span>ホームページを制作したい</span></li>
				<li><i class="fa fa-check-circle"></i><span>採用活動をしているが反響がない</span></li>
				<li><i class="fa fa-check-circle"></i><span>話題のインディードを活用してみたい </span></li>
				<li><i class="fa fa-check-circle"></i><span>気軽に相談できる経営パートナーが欲しい</span></li>
				<li><i class="fa fa-check-circle"></i><span>集客や人材採用について専門家に相談してみたい</span></li>
			</ul>
		</div>
	</div>

</section>
-->


<section class="topStaff padding" id="system">
	<div class="container">
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">外国人技能実習生受入制度とは</h3>
				<p class="mainColor fontEn h4">Foreign Trainee System</p>
			</div>
		</div>
		<div class="row mb30">
			<div class="col-sm-6 col-sm-push-6">
				<div class="mb30">
				
<p>外国人技能実習制度は、日本の企業において発展途上国の若者を技能実習生として受け入れ、実際の実務を通じて実践的な技術や技能・知識を学び、帰国後母国の経済発展に役立ててもらうことを目的とした公的制度です。</p>


                    

				</div>
			</div>
			<div class="col-sm-6 col-sm-pull-6" data-aos="fade-right">
				<img src="<?php echo get_template_directory_uri();?>/img/top_service_01.jpg" alt="外国人技能実習制度は、日本の企業において発展途上国の若者を技能実習生として受け入れ、実際の実務を通じて実践的な技術や技能・知識を学び、帰国後母国の経済発展に役立ててもらうことを目的とした公的制度です。">
			</div>
		</div>
		<div class="row mb30">
			<div class="col-sm-6">
				<div class="mb30">
				
<p>一般的に受入れ可能職種に該当する企業様は、当組合のような監理団体を通じて技能実習生を受け入れることができます。</p>
<p>入国した実習生は、実習実施機関（受入れ企業様）と雇用関係を結び、実践的な能力を高めるために3年間の技能実習に入ります。</p>
				</div>
			</div>
			<div class="col-sm-6" data-aos="fade-left">
				<img src="<?php echo get_template_directory_uri();?>/img/top_service_02.jpg" alt="一般的に受入れ可能職種に該当する企業様は、当組合のような監理団体を通じて技能実習生を受け入れることができます。">
			</div>
		</div>
		<div class="row mb30">
			<div class="col-sm-6 col-sm-push-6">
				<div class="mb30">
				
<p>原則として3年間の在留が認められ、最初の1年間の資格を「技能実習1号」、あとの2年間の資格を「技能実習2号」と区分します。</p>
				</div>
			</div>
			<div class="col-sm-6 col-sm-pull-6" data-aos="fade-right">
				<img src="<?php echo get_template_directory_uri();?>/img/top_service_03.jpg" alt="原則として3年間の在留が認められ、最初の1年間の資格を「技能実習1号」、あとの2年間の資格を「技能実習2号」と区分します。">
			</div>
		</div>
	</div>
</section>
    

<section class="pageCommonFee padding bgSubColor" id="flow">
	<div class="container">
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">制度利用の流れ</h3>
				<p class="mainColor fontEn h4">Flow</p>
			</div>
		</div>
		<div class="row mb50">
			<div class="col-sm-6">
                <h4 class="mainColor bold h3 text-center mb30">受入企業様</h4>
				<div class="pageCommonFeeBox mb30">
					<h5 class="mainColor h5 bold fontEn">STEP 01</h5>
					<p class="pageFeeColor h4 bold">受入のご相談・組合加入</p>
				</div>
				<div class="pageCommonFeeBox mb30">
					<h5 class="mainColor h5 bold fontEn">STEP 02</h5>
					<p class="pageFeeColor h4 bold">書類選考・現地面接・採用</p>
				</div>
				<div class="pageCommonFeeBox mb30">
					<h5 class="mainColor h5 bold fontEn">STEP 03</h5>
					<p class="pageFeeColor h4 bold">必要書類、寮のご準備など</p>
				</div>
				<div class="pageCommonFeeBox mb30">
					<h5 class="mainColor h5 bold fontEn">STEP 04</h5>
					<p class="pageFeeColor h4 bold">実習スタート</p>
				</div>
			</div>
			<div class="col-sm-6">
                <h4 class="mainColor bold h3 text-center mb30">アシスト・ジャパン</h4>
				<div class="pageCommonFeeBox mb30">
					<h5 class="mainColor h5 bold fontEn">STEP 01</h5>
					<p class="pageFeeColor h4 bold">実習候補生確保・説明相談</p>
				</div>
				<div class="pageCommonFeeBox mb30">
					<h5 class="mainColor h5 bold fontEn">STEP 02</h5>
					<p class="pageFeeColor h4 bold">実習生募集・面接同行</p>
				</div>
				<div class="pageCommonFeeBox mb30">
					<h5 class="mainColor h5 bold fontEn">STEP 03</h5>
					<p class="pageFeeColor h4 bold">書類準備、各種講習の実施</p>
				</div>
				<div class="pageCommonFeeBox mb30">
					<h5 class="mainColor h5 bold fontEn">STEP 04</h5>
					<p class="pageFeeColor h4 bold">実習スタート後のフォロー</p>
				</div>
			</div>
        </div>
	</div>
</section>

<section class="topQa padding">
	<div class="container">
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">受入人数</h3>
				<p class="mainColor fontEn h4">Number of People</p>
			</div>
		</div>
        <div class="width720">

            <table class="table table-bordered">
                <tr>
                    <th>受入企業の常勤職員数</th>
                    <th>1年間で受入れ可能な技能実習生の人数</th>
                </tr>
                <tr>
                    <td>301人以上</td>
                    <td>常勤職員の20分の1</td>
                </tr>
                <tr>
                    <td>201人以上300人以下</td>
                    <td>15人以内</td>
                </tr>
                <tr>
                    <td>101人以上200人以下</td>
                    <td>10人以内</td>
                </tr>
                <tr>
                    <td>51人以上100人以下</td>
                    <td>6人以内</td>
                </tr>
                <tr>
                    <td>50人以下</td>
                    <td>3人以内</td>
                </tr>
            </table>        
        </div>
    </div>
</section>





<section class="topQa padding" id="qa">
	<div class="container">
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">よくあるご質問</h3>
				<p class="mainColor fontEn h4">Q&amp;A</p>
			</div>
		</div>
		<div class="topQaBox">
			<dl class="topQaBoxDl">
				<dt class="tra">
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">Q</li>
						<li><hr></li>
						<li>受入時の注意点は？</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">A</li>
						<li><hr></li>
						<li>文化の違いによる勘違いなどからくる相互不信があります。日本語が出来る実習生も聞き違いや思い違いにより言った事をやらない。（言われていないのでやらない）ことも。しっかりコミュニケーションをはかってください。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="topQaBoxDl">
				<dt class="tra">
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">Q</li>
						<li><hr></li>
						<li>実習生の住まいはこちらで準備するの？</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">A</li>
						<li><hr></li>
						<li>はい。受け入れ企業様には生活に必要な居住場所をご用意いただきます。実習生は日本に入国し貴社の寮に入った時点では洗濯機や冷蔵庫といった生活に必要な設備を用意できませんので、そのような設備の整った寮などを企業様にてご用意いただきます。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="topQaBoxDl">
				<dt class="tra">
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">Q</li>
						<li><hr></li>
						<li>賃金はどう決めるの？</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">A</li>
						<li><hr></li>
						<li>実習生は労働関係法令上の「労働者」となります。最低賃金の適用対象となりますのでご注意下さい。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="topQaBoxDl">
				<dt class="tra">
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">Q</li>
						<li><hr></li>
						<li>どんなことが不正行為になりますか？</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">A</li>
						<li><hr></li>
						<li>実習生は入管法上での在留資格は”技能実習生”となります。しかしそれだけではなく労働基準法に照らし合わせ”労働者”として扱われます。入管法や労働基準法に違反した行為（資格外実習の実施や最低賃金割れ、社会保険の未加入等）は全て不正行為となります。不正行為と認定された場合は、実習生の即時帰国や今後受け入れ停止等、厳しい措置がとられますのでご注意下さい。</li>
					</ul>
				
				</dd>
			</dl>
		</div>
	</div>
</section>

<section class="topNews padding bgSubColor" id="news">
	<div class="container">
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">お知らせ</h3>
				<p class="mainColor fontEn h4">News</p>
			</div>
		</div>
		<div class="topNewsBox mb50" data-aos="fade-up">
		
			<?php
				//$paged = (get_query_var('page')) ? get_query_var('page') : 1;
				$paged = get_query_var('page');
				$args = array(
					'post_type' =>  'post', // 投稿タイプを指定
					'paged' => $paged,
					'posts_per_page' => 6, // 表示するページ数
					'orderby'=>'date',
					/*'cat' => -5,*/
					'order'=>'DESC'
							);
				$wp_query = new WP_Query( $args ); // クエリの指定 	
				while ( $wp_query->have_posts() ) : $wp_query->the_post();
					//ここに表示するタイトルやコンテンツなどを指定 
				get_template_part('content-post-top'); 
				endwhile;
				//wp_reset_postdata(); //忘れずにリセットする必要がある
				wp_reset_query();
			?>		
		</div>
		<a href="<?php echo home_url();?>/news" class="button white tra text-center">詳しく見る</a>
	</div>
</section>


<!--
<iframe class="company_map" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15863.817665854744!2d133.69209637681337!3d34.63356314947796!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc59f5bbbd7736eec!2z77yI5qCq77yJ5LiJ5rW344Oi44O844K_44O844Kz44O844Od44Os44O844K344On44Oz!5e0!3m2!1sja!2sjp!4v1559098322291!5m2!1sja!2sjp" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
-->
    

</main>




<?php get_footer(); ?>