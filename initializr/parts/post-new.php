		  <?php $args = array(
			'numberposts' => 5, //表示する記事の数
			'post_type' => 'post' //投稿タイプ名
			// 条件を追加する場合はここに追記
		  );
		  $customPosts = get_posts($args);
		  if($customPosts) : foreach($customPosts as $post) : setup_postdata( $post );
		  ?>
		  
<?php
	$category = get_the_category();
	$cat_id   = $category[0]->cat_ID;
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
?>

<!-- カテゴリーIDを表示したい所に -->
<?php //echo $cat_id; ?>

<!-- カテゴリー名を表示したい所に -->
<?php //echo $cat_name; ?>

<!-- カテゴリースラッグを表示したい所に -->
<?php //echo $cat_slug; ?>
		  
		  
			<li class="clearfix">
				<div class="sideArt__img">
					<a href="<?php the_permalink();?>" class="">
					<?php if (has_post_thumbnail()): ?>
<?php 
	// アイキャッチ画像のIDを取得
	$thumbnail_id = get_post_thumbnail_id(); 
	// mediumサイズの画像内容を取得（引数にmediumをセット）
	$eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
	$eye_img_s = wp_get_attachment_image_src( $thumbnail_id , 'thumb_size_s_false',false );
?>
									<div class="topArt__img bgup " style="background-image:url(<?php echo $eye_img_s[0];?>);"></div><?php else: ?>
									<div class="topArt__img bgup " style="background-image:url(<?php echo get_template_directory_uri();?>/img/thumb_sample.png);"></div><?php endif; ?></a>
				</div>
				<div class="sideArt__text">
					<div class="topArt__art side">
						<!--<p>
							<span class="cate white text_ss <?php echo $cat_slug; ?>"><?php echo $cat_name; ?></span>
							<span class="day text_m"><?php echo get_the_date();?></span>
						</p>-->
						<a href="<?php the_permalink(); ?>" class="">
							<h4 class="artTitle  text_s mb10 opa"><?php the_title(); ?></h4>
						</a>
					</div>
				</div>
			</li>
		  
		  <?php endforeach; ?>
		  <?php else : //記事が無い場合 ?>
		  <p>記事がありません</p>
		  <?php endif;
		  wp_reset_postdata(); //クエリのリセット ?>
