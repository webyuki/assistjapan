<section class="sectionFlow mb50"  id="serviceflow">
	<div class="container">
		<div class="text-center mb50" data-aos="fade-up">
			<h3 class="titleJp mColor h3 titleBd">施術の流れ</h3>
			<p class="titleEn subColor questrial">FLOW</p>
		</div>
		<div class="row" data-aos="fade-up">
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative clearfix floatItems">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">01</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/flow01.jpg" alt="まずはお電話にて予約をお取りください">
					<p class="mb10">まずはお電話にて予約をお取りください</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative clearfix floatItems">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">02</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/flow02.jpg" alt="症状、お悩み、不安な点などを丁寧にヒアリングさせていただきます">
					<p class="mb10">症状、お悩み、不安な点などを丁寧にヒアリングさせていただきます</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative clearfix floatItems">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">03</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/flow03.jpg" alt="骨格のゆがみや筋肉の緊張などが一目で分かる３D姿勢分析機器「Peek A Body」を使い、症状を客観的に分析します">
					<p class="mb10">骨格のゆがみや筋肉の緊張などが一目で分かる３D姿勢分析機器「Peek A Body」を使い、症状を客観的に分析します</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative clearfix floatItems">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">04</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/flow04.jpg" alt="ヒアリングと分析の結果から、施術内容についてご相談します">
					<p class="mb10">ヒアリングと分析の結果から、施術内容についてご相談します</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative clearfix floatItems">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">05</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/flow05.jpg" alt="施術に入ります。手技・機器などによって体のバランスを整えて痛みを緩和していきます">
					<p class="mb10">施術に入ります。手技・機器などによって体のバランスを整えて痛みを緩和していきます<br><span class="text_m">※症状によって手技療法、超音波、鍼、灸、筋肉の症状改善を早める「グラストンテクニック」など施術内容は多岐にわたります</span></p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative clearfix floatItems">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">06</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/recruit2.jpg" alt="施術終了。次回の予約を取っていただきます。症状が解消するまでしっかりサポートいたします">
					<p class="mb10">施術終了。次回の予約を取っていただきます。症状が解消するまでしっかりサポートいたします</p>
				</div>
			</div>
		
		</div>
	</div>
</section>
