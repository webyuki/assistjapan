		<!--
		<div class="titleCommon mb30">
			<span class="sweetpineapple">About</span>
			<span class="hannari">このサイトについて</span>
		</div>
		-->
		<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/about_photo.jpg" alt="">
		<p class="sideAbout__by"><a href="https://twitter.com/d_log8" target="_blank">JANO (@d_log8)</a></p>
		<p class="sideAbout__text">webで何でもできると思ってる効率化バカ。<br><br>

効率化を求めるあまり<a href="http://webshugi.com/category/coffee/">コーヒーを自分で焙煎しだしたり</a>、<a href="http://webshugi.com/jisui-books/">本を裁断して電子化したり</a>して遊んでます。<br>
ミニマリズムの概念は好きですが、家にモノは普通にあるし<a href="http://webshugi.com/category/fashion/">服が好き</a>です。<br><br>

「どんな生活環境でも生きていける」ことを目標に半年後に<a href="http://webshugi.com/world-trip/">世界一周ノマド生活</a>をしてきます。</p>
		<?php get_template_part('parts/post-sns'); ?>

