<?php get_header(); ?>



<section class="area_single">
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<div class="entry">
					<?php get_template_part( 'parts/breadcrumb' ); ?>				
					<div class="title_bg title_margin">
						<h2 class="h2 title_main  bold"><?php post_type_archive_title(); ?></h2>
					</div>
					<div class="wrapper_thumb">
						<ul>
							<?php 
								$paged = get_query_var('paged');
								query_posts($query_string . '&posts_per_page=12&paged=' . $paged);
								while ( have_posts() ) : the_post();
									get_template_part('content','example'); 
								endwhile ;
							?>
						</ul>
					</div>
					<?php get_template_part( 'parts/pagenation' ); ?>				
				</div>
			</div>
			<div class="col-sm-3">
				<?php //get_sidebar(); ?>
                <?php get_sidebar(example2); ?>
   			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>