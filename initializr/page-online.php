<?php get_header(); ?>

<main>

<section class="bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_online_fv.jpg');">
    <div class="pageOnlineFvWrap padding">
        <div class="container">
            <div class="text-center">
                <div class="inlineBlock white">
                    <h3 class="h3 bold titleBdWhite mb10">無料オンライン相談</h3>
                    <p class="fontEn h4">Online Consultation</p>
                </div>
            </div>
        </div>
    </div>
</section>




<section class="margin">
    <div class="container">
       
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">非対面で相談できるオンライン相談</h3>
				<p class="mainColor fontEn h4">About</p>
			</div>
		</div>
       
        <div class="width720">
            <p class="">アシストジャパンの「オンライン相談」は、担当スタッフと同じ画面をお客さまのパソコンもしくはモバイル端末の画面に表示させ、ご説明させていただくビデオ通話サービスです。</p>
            <p>同じ画面を見ながらご説明いたしますので、会話だけでは伝わらない情報もお伝えする事ができます。</p>
            <p>安心のオンライン相談でアシストジャパンのサービスの疑問や不安を解消してみませんか？</p>
        </div>
    </div>
</section>

<section class="padding bgMainColorLight">
    <div class="container">
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">オンライン相談の3つの特徴</h3>
				<p class="mainColor fontEn h4">Merit</p>
			</div>
		</div>
        <div class="row">
            <div class="col-sm-4">
                <div class="pageRecruitWantedBox mb30">
                    <img src="<?php echo get_template_directory_uri();?>/img/page_online_merit_01.png" alt="素直な人" class="pageRecruitWantedIco mb10">
                    <h4 class="bold mainColorLight h4 text-center mb30">移動しなくても<br class="notTab">安心して手軽に相談できます</h4>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageRecruitWantedBox mb30">
                    <img src="<?php echo get_template_directory_uri();?>/img/page_online_merit_02.png" alt="あいさつができる人" class="pageRecruitWantedIco mb10">
                    <h4 class="bold mainColorLight h4 text-center mb30">写真や資料でわかりやすく<br class="notTab">丁寧にご説明します</h4>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="pageRecruitWantedBox mb30">
                    <img src="<?php echo get_template_directory_uri();?>/img/page_online_merit_03.png" alt="前向きな人" class="pageRecruitWantedIco mb10">
                    <h4 class="bold mainColorLight h4 text-center mb30">PC・スマホ・タブレット全ての<br class="notTab">デバイスにご対応しています</h4>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="margin">
    <div class="container">
        <div class="text-center mb30">
            <h3 class="h3 bold mainColor">オンライン相談でできること</h3>
        </div>
        <div class="width720 mb50">
            <p class="">「電話やメールのやり取りだけでの契約は不安…」そんなお客様も安心していただけるように、対応いたします。</p>
            <p>リアルタイムで担当者とやり取りができるので、分からないことをより詳しく確認いただくことができます。</p>
        </div>
        <div class="row mb30" data-aos="fade-up">
            <div class="col-sm-3">
                <div class="pageReformPointBox mb30">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_online_service_05.jpg" alt="外国人実習生の活用のご相談">
                    <h4 class="h4 bold text-center mb10">外国人実習生の活用の<br class="pc">ご相談</h4>
                    <!--<p class="text_m gray">一緒に画面を見ながら、お客様に納得いただける見積りをご提示いたします。</p>-->
                </div>
            </div>
            <div class="col-sm-3">
                <div class="pageReformPointBox mb30">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_online_service_06.jpg" alt="外国人実習生の派遣のご相談">
                    <h4 class="h4 bold text-center mb10">外国人実習生の派遣の<br class="pc">ご相談</h4>
                    <!--<p class="text_m gray">一緒に画面を見ながら、お客様に納得いただける見積りをご提示いたします。</p>-->
                </div>
            </div>
            <div class="col-sm-3">
                <div class="pageReformPointBox mb30">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_online_service_07.jpg" alt="ご提案やアドバイス">
                    <h4 class="h4 bold text-center mb10">ご提案や<br class="notTab">アドバイス</h4>
                    <!--<p class="text_m gray">一緒に画面を見ながら、お客様に納得いただける見積りをご提示いたします。</p>-->
                </div>
            </div>
            
            <div class="col-sm-3">
                <div class="pageReformPointBox mb30">
                    <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_online_service_01.jpg" alt="画面共有にてお見積りのご提案">
                    <h4 class="h4 bold text-center mb10">画面共有にて<br class="notTab">お見積りのご提案</h4>
                    <!--<p class="text_m gray">一緒に画面を見ながら、お客様に納得いただける見積りをご提示いたします。</p>-->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="margin">
    <div class="container">
        <div class="width870">
            <div class="bgMainColorLight pageOnlineAboutWrap">
                <div class="row mb30">
                    <div class="col-sm-6">
                        <div class="mb30">
                            <h3 class="h3 bold mainColorLight">オンライン相談サービスとは</h3>
                        </div>
                        <p class="">「zoomまたはスカイプというサービスを使います。(通信費用が生じるためWi-Fiでの接続を推奨します)</p>
                        <p>アプリのダウンロードやアカウントの登録は不要です。(サービスの利用は無料です)</p>
                        <p>パソコンの場合、カメラとマイクが別途必要となります。お持ちでない方はスマートフォン・タブレットでのご利用をおすすめします。</p>
                    </div>
                    <div class="col-sm-6">
                        <img class="mb10" src="<?php echo get_template_directory_uri();?>/img/page_online_about.jpg" alt="">
                    </div>
                </div>
                <div class="text-center mb30">
                    <h3 class="h3 bold">オンライン相談で利用可能なソフト</h3>
                </div>
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-5">
                        <div class="text-center mb30">
                            <img class="mb10 pageOnlineIcoSoft" src="<?php echo get_template_directory_uri();?>/img/page_online_ico_zoom.png" alt="">
                            <p class="h4 mb20 bold">Zoom</p>
                            <a href="https://play.google.com/store/apps/details?id=us.zoom.videomeetings" target="_blank">
                                <img class="mb10 pageOnlineBannerApp" src="<?php echo get_template_directory_uri();?>/img/page_online_banner_google.png" alt="">

                            </a>
                            <a href="https://apps.apple.com/us/app/id546505307" target="_blank">
                                <img class="mb10 pageOnlineBannerApp" src="<?php echo get_template_directory_uri();?>/img/page_online_banner_app.png" alt="">
                            </a>
                            <a href="https://zoom.us/download" target="_blank" class="linkA mainColorLight bold text_m">パソコンでダウンロード</a>
                            
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="text-center mb30">
                            <img class="mb10 pageOnlineIcoSoft" src="<?php echo get_template_directory_uri();?>/img/page_online_ico_skype.png" alt="">
                            <p class="h4 mb20 bold">Skype</p>
                            <a href="https://play.google.com/store/apps/details?id=com.skype.raider&referrer=utm_source%3Dscomphonedownload" target="_blank">
                                <img class="mb10 pageOnlineBannerApp" src="<?php echo get_template_directory_uri();?>/img/page_online_banner_google.png" alt="">

                            </a>
                            <a href="https://apps.apple.com/app/apple-store/id304878510" target="_blank">
                                <img class="mb10 pageOnlineBannerApp" src="<?php echo get_template_directory_uri();?>/img/page_online_banner_app.png" alt="">
                            </a>
                            <a href="https://www.skype.com/ja/get-skype/" target="_blank" class="linkA mainColorLight bold text_m">パソコンでダウンロード</a>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="padding bgSubColor">
    <div class="container">
        <div class="text-center mb50">
            <p class="fontEn h4 mb0 mainColor">FLOW</p>
            <h3 class="h3 bold">オンライン相談の流れ</h3>
        </div>
        <div class="width980 bgWhite pageReformFlowWrap" data-aos="fade-up">
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_online_flow_01.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">オンライン相談のお申し込み</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 01</p>
                    <p>下記のフォームよりご希望の日時・ご相談内容を入力の上、お申し込みください。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_online_flow_02.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">ご案内メールを受け取る</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 02</p>
                    <p>担当スタッフより、オンライン相談日のご案内メールを差し上げます。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_online_flow_03.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">担当スタッフからの開始直前のお電話</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 03</p>
                    <p>当日は、開始直前に担当スタッフよりお電話にてご連絡いたします。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-3">
                    <div class="pageReformFlowImg bgImg bdBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/page_online_flow_04.jpg');"></div>
                </div>
                <div class="col-sm-9">
                    <h4 class="topServiceTitle relative bold h4 mb10"><span class="bold">オンライン相談ツールの起動</span></h4>
                    <p class="fontEn text_m mainColor mb30">STEP 04</p>
                    <p>無料ツール【ZOOM】・【SKYPE】のどちらかをご利用いただけます。上記より事前にダウンロードしてください。</p>
                </div>
            </div>
        
        </div>
    </div>
</section>

<section class="margin">
	<div class="container">
		<div class="">
			<div class="contInCont" data-aos="fade-up">
                <div class="text-center mb50">
                    <p class="fontEn h4 mb0 mainColor">RESERVATION</p>
                    <h3 class="h3 bold">お申込みはこちらから</h3>
                </div>
				<div class="contactForm" data-aos="fade-up"><?php echo do_shortcode('[mwform_formkey key="53"]'); ?></div>
			</div>
		</div>
	</div>
</section>


<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>







<?php get_footer(); ?>