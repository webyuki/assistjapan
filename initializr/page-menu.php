<?php
/*
Template Name: わたしたちのできること
*/
?>

<?php get_header(); ?>
<div class="pageFv">
	<div class="container">
		<?php get_template_part( 'parts/page-fv' ); ?>
	</div>
</div>
<?php $slug = $post -> post_name; ?>

<section class="area_single">
	<div class="container">
		<?php get_template_part( 'parts/breadcrumb' ); ?>				
		<div class="row">
			<div class="col-sm-9">
				<div class="entry">
					<?php
						$args = array(
							'post_type' =>  'menu', // 投稿タイプを指定
							'posts_per_page' => 1, // 表示するページ数
							'order'=>'ASC',
							'orderby'=>'menu_order',
							'tax_query' => array(
								array(
									'taxonomy' => 'menu_tax',
									'field' => 'slug',
									'terms' => $slug,
									/*'operator'  => 'NOT IN'*/
								)	
							),								
						);
						$wp_query = new WP_Query( $args ); // クエリの指定 	
						while ( $wp_query->have_posts() ) : $wp_query->the_post();
							//ここに表示するタイトルやコンテンツなどを指定 
						get_template_part('content','post'); 
				
						endwhile;
						wp_reset_postdata(); //忘れずにリセットする必要がある
					?>		
				<?php get_template_part( 'parts/postlink' ); ?>				
					
				</div>
				<?php //get_template_part( 'parts/pagenation' ); ?>				
			</div>
			<div class="col-sm-3">
				<?php //get_sidebar(); ?>
                <?php get_sidebar('menu'); ?>
                
   			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>




