<section class="footerContact margin" data-aos="fade-up">
	<div class="container">
		<div class="bgMainColor footerContactBox">
            <div class="text-center mb50">
                <div class="inlineBlock white">
                    <h3 class="h3 bold titleBdWhite mb10">お問い合わせ</h3>
                    <p class="fontEn h4">Contact</p>
                </div>
            </div>
            <div class="mb50">
                <div class="row">
                    <div class="col-sm-4">
                        <a href="<?php echo home_url();?>/contact" class="button buttonBgWhite bold mb10 white tra text-center mainColor">お問い<br class="visible-sm-inline">合わせ</a>
                    </div>
                    <div class="col-sm-4">
                        <a href="<?php echo home_url();?>/online" class="button buttonBgWhite bold mb10 white tra text-center mainColor">オンライン<br class="visible-sm-inline">相談</a>
                    </div>
                    <div class="col-sm-4">
                        <a href="https://line.me/R/ti/p/%40682itjit" target="_blank" class="button buttonBgLine bold white tra text-center white">LINE@で<br class="visible-sm-inline">相談する</a>
                    </div>
                </div>
                <a href="tel:0863331928" class="text-center fontEn h1 white"><p>0863-33-1928</p></a>
            </div>
            <div class="text-center mb20">
                <div class="inlineBlock white">
                    <h3 class="h4 bold titleBdWhite">事業案内ダウンロード</h3>
                </div>
            </div>
            <div class="mb50">
                <a href="<?php echo get_template_directory_uri();?>/file/document.pdf" target="_blank" class="button buttonBgWhite bold white tra text-center mainColor">ダウンロードする</a>
            </div>
            <!--
            <div class="text-center mb20">
                <div class="inlineBlock white">
                    <h3 class="h4 bold titleBdWhite">LINEでお問い合わせ</h3>
                </div>
            </div>
            <div class="">
                <a href="https://line.me/R/ti/p/%40682itjit" target="_blank" class="button buttonBgLine bold white tra text-center white">LINE@で相談する</a>
            </div>
            -->
		</div>
	</div>
</section>

<section class="sectionPankuzu">
	<div class="container">
		<?php get_template_part( 'parts/breadcrumb' ); ?>				
	</div>
</section>






<footer class="text_m text-left bgMainColor white footerCont">
	<div class="container">
		<div class="text-center">
			<p class="">アシスト・ジャパン協同組合</p>
			<a href="<?php echo home_url();?>"><p class="logo logo_footer remove mb10"><?php the_title();?></p></a>
		
			<p class="mb30 grayClolor">〒706-0001<br>岡山県玉野市田井3丁目5681番地12<!--<a target="_blank" href="https://goo.gl/maps/iX4Codw733rkjkou9"><i class="fa fa-map-marker"></i></a>--><br>
					TEL　0863-33-1928</p>
            <!--
            <a href="https://line.me/R/ti/p/%40682itjit" target="_blank">
                <img src="<?php echo get_template_directory_uri();?>/img/line_button.png" alt="" class="lineButton mb50">
            </a>
            -->
		</div>
	</div>
			<div class="wrapper_copy">
		<div class="container">
			<p class="text-center text_ss">copyright© 2019 assist japan all rights reserved.</p>
		</div>
	</div>
</footer>
<?php wp_footer(); ?>
<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>



<script>
AOS.init({
	placement	: "bottom-top",
	duration: 1000,
});
</script>
</body>
</html>
 