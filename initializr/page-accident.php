<?php get_header(); ?>
<main>
<section class="pageHeader bgCenter mb30">
	<div class="pageHeaderCover">
		<div class="container">
			<div class="text-center white pageHeaderTitle">
				<h3 class="titleJp h3 titleBd"><?php the_title();?></h3>
				<p class="titleEn questrial"><?php $post = get_page($page_id); echo $post->post_name;?></p>
			</div>
		</div>
	</div>
</section>
<section class="pageNavi mb50">
	<div class="container">
		<ul class="inline_block pageNaviUl text-center white tra text_m">
			<li><a href="#accident">交通事故治療とは？</a>
			<li><a href="#probrem">こんなお悩みはありませんか？</a>
			<li><a href="#serviceflow">施術の流れ</a>
			<li><a href="#detail">各種対応</a>
			<li><a href="#notice">知っておいてほしいこと</a>
			</li>
		</ul>
	</div>
</section>

<section class="mb50" id="accident">
	<div class="container">
		<div class="text-center mb50">
			<h3 class="titleJp mColor h3 titleBd">交通事故治療とは？</h3>
			<p class="titleEn subColor questrial">What's accident?</p>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<h4 class="h4 bold mb30">交通事故に遭われた場合、自賠責保険を適用することで自己負担ナシ（0円）で治療を受けることが可能です</h4>
				<p>交通事故に遭われたら、まず病院や整形外科に行ってください。</p>
				<p>そこでお体の状態をなるべく細かく説明して指示に従って下さい。</p>
				<p>病院や整形外科に通いながら整骨院の治療も併用することで、より症状の改善を早めることが可能です。</p>
				<p>当院では不運にも交通事故に遭われた方の早い症状改善のお手伝いをさせて頂きたいと考えています。</p>
		</div>
			<div class="col-sm-6">
				<img class="helloImg" src="<?php echo get_template_directory_uri();?>/img/acci_01.jpg" alt="交通事故に遭われた場合、自賠責保険を適用することで自己負担ナシ（0円）で治療を受けることが可能">
			</div>
		</div>
	</div>
</section>




<section class="topConcept sectionCheck100 mb50" id="probrem">
	<div class="container">
		<div class="text-center mb50" data-aos="fade-up">
			<h3 class="titleJp mColor h3 titleBd">こんなお悩みありませんか？</h3>
			<p class="titleEn subColor questrial">Probrem</p>
		</div>
		<div class="row" data-aos="fade-up">
			<div class="col-sm-7">
				<div class="checkWith100 mb30">
					<ul class="inline_block footerAppealCheck h4 bold">
						<li><i class="fa fa-check-circle"></i>病院では薬と湿布だけで治療がない</li>
						<li><i class="fa fa-check-circle"></i>待ち時間はとても長いのに治療時間は短い</li>
						<li><i class="fa fa-check-circle"></i>担当の先生とコミュニケーションが取りにくい</li>
						<li><i class="fa fa-check-circle"></i>仕事や家庭の都合で、もっと近場で治療を受けたい</li>
						<li><i class="fa fa-check-circle"></i>通院しているが症状が改善しない</li>
						<li><i class="fa fa-check-circle"></i>キチンとした治療をしてくれてるのか不安</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-5">
				<img class="mb30" src="<?php echo get_template_directory_uri();?>/img/acci_03.jpg" alt="病院では薬と湿布だけで治療がない">
			</div>
		</div>
		
	</div>
</section>


<?php //get_template_part( 'parts/page-flow' ); ?>				
<!--内容が異なるため共通化にせず-->

<section class="sectionFlow mb50"  id="serviceflow">
	<div class="container">
		<div class="text-center mb50" data-aos="fade-up">
			<h3 class="titleJp mColor h3 titleBd">施術の流れ</h3>
			<p class="titleEn subColor questrial">FLOW</p>
		</div>
		<div class="row" data-aos="fade-up">
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative clearfix floatItems">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">01</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/flow01.jpg" alt="まずはお電話にて予約をお取りください">
					<p class="mb10">まずはお電話にて予約をお取りください</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative clearfix floatItems">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">02</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/flow02.jpg" alt="症状、お悩み、不安な点などを丁寧にヒアリングさせていただきます">
					<p class="mb10">症状、お悩み、不安な点などを丁寧にヒアリングさせていただきます</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative clearfix floatItems">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">03</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/flow04.jpg" alt="ヒアリングと分析の結果から、施術内容についてご相談します">
					<p class="mb10">ヒアリングと分析の結果から、施術内容についてご相談します</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative clearfix floatItems">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">04</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/flow05.jpg" alt="施術に入ります。手技・機器などによって体のバランスを整えて痛みを緩和していきます">
					<p class="mb10">施術に入ります。手技・機器などによって体のバランスを整えて痛みを緩和していきます<br><span class="text_m">※症状によって手技療法、超音波、鍼、灸、筋肉の症状改善を早める「グラストンテクニック」など施術内容は多岐にわたります</span></p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative clearfix floatItems">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">05</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/recruit2.jpg" alt="施術終了。次回の予約を取っていただきます。症状が解消するまでしっかりサポートいたします">
					<p class="mb10">施術終了。次回の予約を取っていただきます。症状が解消するまでしっかりサポートいたします。また当院にて保険会社との対応もいたします。</p>
				</div>
			</div>
		
		</div>
	</div>
</section>






<section class="topStrong bgImgBlur relative mb50" id="detail">
	<div class="whiteBg">
		<div class="container">
			<div class="text-center">
				<h3 class="titleJp h3 titleBd">各種対応</h3>
			</div>
			<div class="row" data-aos="fade-up">
				<div class="col-sm-3">
					<div class="point3 matchHeight">
						<div class="point3Number flex justCenter alignCenter">
							<div class="h3 italic mColor bold">01</div>
						</div>
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/acci_06.jpg" alt="自賠責保険、任意保険適用可">
						<h4 class="h4 bold text-center">自賠責保険、任意保険適用可</h4>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="point3 matchHeight">
						<div class="point3Number flex justCenter alignCenter">
							<div class="h3 italic mColor bold">02</div>
						</div>
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/acci_05.jpg" alt="保険会社との対応">
						<h4 class="h4 bold text-center">保険会社との対応</h4>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="point3 matchHeight">
						<div class="point3Number flex justCenter alignCenter">
							<div class="h3 italic mColor bold">03</div>
						</div>
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/housecallsservice01.jpg" alt="徹底した個別対応施術">
						<h4 class="h4 bold text-center">徹底した個別対応施術</h4>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="point3 matchHeight">
						<div class="point3Number flex justCenter alignCenter">
							<div class="h3 italic mColor bold">04</div>
						</div>
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/topprobrem05.jpg" alt="入院後のリハビリ通院OK">
						<h4 class="h4 bold text-center">入院後のリハビリ通院OK</h4>
					</div>
				</div>
				
			</div>
		</div>
	
	</div>
</section>

<section class="mb50" id="notice">
	<div class="container">
		<div class="text-center mb50">
			<h3 class="titleJp mColor h3 titleBd">知っておいてほしいこと</h3>
			<p class="titleEn subColor questrial">Notice</p>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<img class="helloImg" src="<?php echo get_template_directory_uri();?>/img/acci_02.jpg" alt="交通事故のケガは些細でも生活に支障がでます">
			</div>
			<div class="col-sm-6">
				<h4 class="h4 bold mb30">交通事故のケガは些細でも生活に支障がでます</h4>
				<p>病院ができることと整骨院でできることには違いがあります。ですので交通事故に遭われたあなたのような人は両方をうまく利用することが一番いいことが分かると思います。</p>
				<p>交通事故・むち打ちの施術は当院にお任せください。</p>
				<p>整骨院ならでは安心・確実の施術をいたします。</p>
		</div>
		</div>
	</div>
</section>






<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>