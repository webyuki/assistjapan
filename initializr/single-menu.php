<?php get_header(); ?>
<!-- Main jumbotron for a primary marketing message or call to action -->


<div class="pageFv">
	<div class="container">
		<?php get_template_part( 'parts/page-fv' ); ?>
	</div>
</div>

<section class="area_single">
	<?php 
		while ( have_posts() ) : the_post();
	?>	
	<div class="container">
		<?php get_template_part( 'parts/breadcrumb' ); ?>				
		<div class="row">
			<div class="col-sm-9">
				<div class="entry">
					<?php get_template_part('content','post'); ?>
					<?php get_template_part( 'parts/postlink' ); ?>						
				</div>
			</div>
			<div class="col-sm-3">
				<?php get_sidebar(); ?>
   			</div>
		</div>
	</div>
	<?php 
		endwhile;
	?>	
</section>
<?php get_footer(); ?>