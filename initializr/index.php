<?php get_header(); ?>
<main>

<section class="padding bgMainColor">
	<div class="container">
		<div class="text-center">
			<div class="inlineBlock white">
				<h3 class="h3 bold titleBdWhite mb10">お知らせ</h3>
				<p class="fontEn h4">News</p>
			</div>
		</div>
	</div>
</section>



<section class="pageNews margin">
	<div class="container">
		<?php get_template_part( 'parts/breadcrumb' ); ?>				
		<div class="row">
			<div class="col-sm-9">
				<?php
					while ( have_posts() ) : the_post();
						get_template_part('content-post'); 
					endwhile;
				?>
			</div>
			<div class="col-sm-3">
				<?php dynamic_sidebar(); ?>
			</div>
		</div>
		<?php get_template_part( 'parts/pagenation' ); ?>
	</div>
</section>
</main>
<?php get_footer(); ?>