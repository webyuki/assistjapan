<?php get_header(); ?>
<main>


<section class="relative topFvSection2">
	<div class="topFv2">
		<div class="main_imgBox2">
			<div class="main_img2" style="background-image:url('<?php echo get_template_directory_uri();?>/img/cp_fv_01.jpg');"></div>
			<div class="main_img2" style="background-image:url('<?php echo get_template_directory_uri();?>/img/cp_fv_03.jpg');"></div>
			<div class="main_img2" style="background-image:url('<?php echo get_template_directory_uri();?>/img/cp_fv_02.jpg');"></div>
		</div>
	</div>
	<div class="topFvBoxText2 white absolute text-center">
		<h2 class="h00 bold mb30">楽しく 必死に 誠実に</h2>
		<p class="fontEn h3">Enjoy,Hard and Sincere</p>
	</div>
</section>


<section class="topGreeding margin" id="concept">
	<div class="container">
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">コンセプト</h3>
				<p class="mainColor fontEn h4">Concept</p>
			</div>
		</div>
		<div class="width720">

<div class="mb30">
	<h4 class="bold titleLine">楽しく、必死に、誠実な仕事を通して、経営者の想いの実現をサポート</h4>
</div>		
	
<p>Ｆ・パートナーズ　取締役社長の井上です。当社ホームページをご覧いただき、ありがとうございます。</p>
<p>当社は売上向上や人材採用を中心としたコンサルティング、チラシやホームページ制作など各種ツールの制作を通して経営支援に取り組む会社です。</p>
<p>社名の「Ｆ」には、Ｆｕｔｕｒｅ（未来）とＦｅｅｌｉｎｇ（想い）の意味を、「パートナーズ」には当社が
クライアントのパートナーであり続けること、また、当社が連携している複数のパートナーとチームで
クライアントの困りごと解決を支援する想いをこめています。</p>
<p>「未来に向かって経営者の想いの実現をサポートする」これが当社の「想い」です。</p>



			<img class="mb30" src="<?php echo get_template_directory_uri();?>/img/cp_10.jpg" alt="" data-aos="fade-up">
			<div class="text-center">
				<p class="text_m mb0 bold">取締役社長</p>
				<p class="bold h3">井上 隆</p>
			</div>
		</div>
		
	</div>
</section>

<section class="topService padding" id="feature">
	<div class="container">
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">当社の特徴</h3>
				<p class="mainColor fontEn h4">Feature</p>
			</div>
		</div>
		<div class="width720 mb30">
		
<p>F・パートナーズは岡山市、倉敷市、総社市など岡山県南エリアを対象に、企業の経営支援に取り組んでいます。</p>
<p>売上向上や人材採用に関する経営コンサルティング、チラシやサイトなどのツール制作支援が主なサービスです。</p>
<p>また税理士や司法書士などの専門家紹介やビジネスマッチング、補助金・助成金などの情報提供など幅広い経営サポートをしています。</p>

		</div>
		<div class="row" data-aos="fade-up">
			<div class="col-sm-4">
				<div class="topServiceBox mb30">
					<div class="topServiceBoxImgBox">
						<div class="topServiceBoxImg bgImg mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/cp_05.jpg');"></div>
					</div>
					<h4 class="bold h4 text-center mb10 mainColor">経営コンサルタントが在籍</h4>
					<p class="text_m gray">当社取締役社長の井上は、大手上場企業、人材サービス会社、商工会議所などで20年近く企業の経営支援に携わってきたキャリアがあります。また経営士、1級販売士、経営支援アドバイザーなど多数の資格も保有しており、経験と知識の双方に裏打ちされたノウハウで貴社の経営を支援します。</p>
					</div>
			</div>
			<div class="col-sm-4">
				<div class="topServiceBox mb30">
					<div class="topServiceBoxImgBox">
						<div class="topServiceBoxImg bgImg mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/cp_04.jpg');"></div>
					</div>
					<h4 class="bold h4 text-center mb10 mainColor">企画からアクションまで一括対応</h4>
					<p class="text_m gray">当社は経営のアドバイスをするだけのコンサルティング会社ではありません。社内に専属デザイナーが在籍しており、プロカメラマンやライター、広告代理店など30社を超えるパートナー企業がいます。そのため、当社を窓口として企画からアクションまで一気通貫で対応が可能です。</p>
					</div>
			</div>
			<div class="col-sm-4">
				<div class="topServiceBox mb30">
					<div class="topServiceBoxImgBox">
						<div class="topServiceBoxImg bgImg mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/cp_06.jpg');"></div>
					</div>
					<h4 class="bold h4 text-center mb10 mainColor">楽しくクリエイティブな仕事を</h4>
					<p class="text_m gray">「楽しく 必死に 誠実に」という当社のワークスタイルを通して、クライアント・パートナー企業・社内でも、常に笑顔でクリエイティブな仕事 ができるよう心がけています。クライアントと共有できる「想い」を大切にし、未来に向かって仕事に取り組みます。</p>
					</div>
			</div>
		</div>
	</div>
</section>

<section class="topTrouble padding bgSubColor">
	<div class="container">
	
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">こんなお悩みはありませんか？</h3>
				<p class="mainColor fontEn h4">Trouble</p>
			</div>
		</div>
		<div class="pageCommonRecommendBox width720" data-aos="fade-up">
			<ul class="inline_block h5">
				<li><i class="fa fa-check-circle"></i><span>名刺やチラシ、パンフレットを制作したい</span></li>
				<li><i class="fa fa-check-circle"></i><span>ホームページを制作したい</span></li>
				<li><i class="fa fa-check-circle"></i><span>採用活動をしているが反響がない</span></li>
				<li><i class="fa fa-check-circle"></i><span>話題のインディードを活用してみたい </span></li>
				<li><i class="fa fa-check-circle"></i><span>気軽に相談できる経営パートナーが欲しい</span></li>
				<li><i class="fa fa-check-circle"></i><span>集客や人材採用について専門家に相談してみたい</span></li>
			</ul>
		</div>
	</div>

</section>



<section class="topStaff padding" id="service">
	<div class="container">
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">サービス内容</h3>
				<p class="mainColor fontEn h4">Service</p>
			</div>
		</div>
		<div class="row mb30">
			<div class="col-sm-6 col-sm-push-6">
				<h4 class="h3 bold titleBd mb30">Promotion</h4>
				<div class="mb30">
				
<p>売上向上につながる販促・集客に関する各種サポートをします。</p>
<p>・チラシ、パンフレット制作<br>・ホームページ制作<br>・新聞折込、ポスティング<br>・地域情報誌掲載<br>・WEB広告<br>・コンサルティング<br>...etc.</p>

				</div>
			</div>
			<div class="col-sm-6 col-sm-pull-6" data-aos="fade-right">
				<img src="<?php echo get_template_directory_uri();?>/img/cp_07.jpg" alt="">
			</div>
		</div>
		<div class="row mb30">
			<div class="col-sm-6">
				<h4 class="h3 bold titleBd mb30">Recruiting</h4>
				<div class="mb30">
				
<p>人材獲得のための採用活動全般をサポートします。</p>
<p>・ハローワーク求人制作<br>・ハリーアップ求人制作<br>・インディード導入、運用<br>・ホームページ制作<br>・人材サービス会社マッチング<br>・コンサルティング<br>...etc.</p>

				</div>
			</div>
			<div class="col-sm-6" data-aos="fade-left">
				<img src="<?php echo get_template_directory_uri();?>/img/cp_08.jpg" alt="">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 col-sm-push-6">
				<h4 class="h3 bold titleBd mb30">Design</h4>
				<div class="mb30">
				
<p>アナログからデジタルまで各種ツール制作をサポートします</p>
<p>・名刺、ショップカード、封筒制作<br>・チラシ、リーフレット、パンフレット制作<br>・のぼり、看板制作<br>・ホームページ制作<br>...etc.</p>

				</div>
			</div>
			<div class="col-sm-6 col-sm-pull-6" data-aos="fade-right">
				<img src="<?php echo get_template_directory_uri();?>/img/fp_08.jpg" alt="">
			</div>
		</div>
	</div>
</section>

<section class="topVoice padding" id="voice">
	<div class="container">
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">お客様の声</h3>
				<p class="mainColor fontEn h4">Voice</p>
			</div>
		</div>
		<div class="row" data-aos="fade-up">
			<div class="col-sm-6">
				<div class="topVoiceBox">
					<ul class="topVoiceBoxUl inlline_block matchHeight mb30">
						<li>
							<img class="" src="<?php echo get_template_directory_uri();?>/img/ico_man.png" alt="無事に抵当権を抹消">
							<!--<p class="text_m mainColor text-center">30代</p>-->
						</li>
						<li>
							<h3 class="fontJpBd mb10 h4">社外に気軽な相談相手ができました</h3>
							<p class="text_m gray">今までは売上や人材のことなど、経営に関する悩みを自分一人で抱えてて誰にも相談できず、実務面でも精神面でも辛い状況に陥ることが多々ありました。Ｆ・パートナーズさん（以下：Ｆ・Ｐ）と顧問契約を結び、社外の経営アドバイザーになっていただきました。社外の方だからこそ、社内の人間には言えないような案件も気軽に相談ができ、また自分が知らない情報や色々なアドバイスをもらうことができ、とても助かりました。</p>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="topVoiceBox">
					<ul class="topVoiceBoxUl inlline_block matchHeight mb30">
						<li>
							<img class="" src="<?php echo get_template_directory_uri();?>/img/ico_woman.png" alt="不動産の相続登記を依頼">
							<!--<p class="text_m mainColor text-center">30代</p>-->
						</li>
						<li>
							<h3 class="fontJpBd mb10 h4">セールの集客をお任せで2倍の結果が</h3>
							<p class="text_m gray">毎年恒例で開催していたセールについて、Ｆ・Ｐさんにプロモーションの相談をしました。今までが大手広告代理店にお願いして新聞折込チラシでＰＲしていましたが、Ｆ・Ｐさんがターゲットや当社の強みを分析してくれ、その結果を反映したチラシを作成してくれ、ポスティングやネット広告まで一括で対応してくれました。その結果、前回よりも2倍の集客・売上を獲得することができました。</p>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="topVoiceBox">
					<ul class="topVoiceBoxUl inlline_block matchHeight mb30">
						<li>
							<img class="" src="<?php echo get_template_directory_uri();?>/img/ico_man.png" alt="父の債権者から借金の督促">
							<!--<p class="text_m mainColor text-center">30代</p>-->
						</li>
						<li>
							<h3 class="fontJpBd mb10 h4">社員数名の当社に念願の若手社員が入社</h3>
							<p class="text_m gray">当社は設立5年目、社員3名の小さな会社です。おかげさまで注文はいくらでもあるのですが、人員が足りず外注に頼ったり、仕事を断っている状態でした。人材採用の必要性は強く感じていたのですが、ハローワークや人材会社に依頼しても全く反響がありませんでした。Ｆ・Ｐさんに相談し求人サポートを一括でお願いしました。ハローワークの求人内容を全て見直し、話題のインディードも導入してくれ、今後も見据え採用に特化したホームページも制作していただけ、結果、30歳の若手社員を採用することができました。</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="pageCommonFee padding bgSubColor">
	<div class="container">
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">料金表</h3>
				<p class="mainColor fontEn h4">Price</p>
			</div>
		</div>
		<div class="row mb30">
			<div class="col-sm-4">
				<div class="pageCommonFeeBox mb30">
					<h5 class="mainColor h4 bold">デザイン名刺作成</h5>
					<p class="pageFeeColor"><span class="h2 bold">2,400</span>円～</p>
					<p class="grayColor text_m pageCommonFeeBoxBorder">※両面カラー、100枚の場合</p>
					<ul class="text_m">
						<li><i class="fa fa-check mainColor"></i>初回のみデザイン費9,000円</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="pageCommonFeeBox mb30">
					<h5 class="mainColor h4 bold">チラシ制作</h5>
					<p class="pageFeeColor"><span class="h2 bold">17,200</span>円～</p>
					<p class="grayColor text_m pageCommonFeeBoxBorder">※Ａ4サイズ、片面カラー、500部の場合</p>
					<ul class="text_m">
						<li><i class="fa fa-check mainColor"></i>企画費、デザイン費、印刷費込</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="pageCommonFeeBox mb30">
					<h5 class="mainColor h4 bold">リーフレット制作</h5>
					<p class="pageFeeColor"><span class="h2 bold">34,000</span>円～</p>
					<p class="grayColor text_m pageCommonFeeBoxBorder">※Ａ3三つ折り、両面カラー、500部の場合</p>
					<ul class="text_m">
						<li><i class="fa fa-check mainColor"></i>企画費、デザイン費、印刷費込</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="row mb30">
			<div class="col-sm-4">
				<div class="pageCommonFeeBox mb30">
					<h5 class="mainColor h4 bold">パンフレット制作</h5>
					<p class="pageFeeColor"><span class="h2 bold">59,000</span>円～</p>
					<p class="grayColor text_m pageCommonFeeBoxBorder">※Ａ3二つ折り、両面カラー、300部の場合</p>
					<ul class="text_m">
						<li><i class="fa fa-check mainColor"></i>企画費、デザイン費、印刷費込</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="pageCommonFeeBox mb30">
					<h5 class="mainColor h4 bold">ホームページ制作</h5>
					<p class="pageFeeColor"><span class="h2 bold">500,000</span>円～</p>
					<p class="grayColor text_m pageCommonFeeBoxBorder"></p>
					<ul class="text_m">
						<li><i class="fa fa-check mainColor"></i>企画費、デザイン費込</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>




<section class="topQa padding" id="qa">
	<div class="container">
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">よくあるご質問</h3>
				<p class="mainColor fontEn h4">Q&amp;A</p>
			</div>
		</div>
		<div class="topQaBox">
			<dl class="topQaBoxDl">
				<dt class="tra">
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">Q</li>
						<li><hr></li>
						<li>初回相談は無料でしょうか？</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">A</li>
						<li><hr></li>
						<li>はい、無料です。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="topQaBoxDl">
				<dt class="tra">
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">Q</li>
						<li><hr></li>
						<li>広告会社とは違うのですか？</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">A</li>
						<li><hr></li>
						<li>はい。当社はコンサルタントとデザイナーが在籍しているため、ビジネス視点でのツール制作が可能です。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="topQaBoxDl">
				<dt class="tra">
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">Q</li>
						<li><hr></li>
						<li>顧問契約など長期的な契約が必須でしょうか？</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">A</li>
						<li><hr></li>
						<li>いいえ。単発での支援も可能です。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="topQaBoxDl">
				<dt class="tra">
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">Q</li>
						<li><hr></li>
						<li>売上向上や人材採用支援以外のサービスもありますか？</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">A</li>
						<li><hr></li>
						<li>30社を越えるビジネスパートナーがいるため、会社設立や法人化支援、保険の見直し、補助金申請など様々なお役立ちが可能です。</li>
					</ul>
				
				</dd>
			</dl>
			<dl class="topQaBoxDl">
				<dt class="tra">
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">Q</li>
						<li><hr></li>
						<li>岡山県外でも対応可能ですか？</li>
					</ul>
				</dt>
				<dd>
					<ul class="topQaBoxUl inlline_block matchHeight">
						<li class="fontEn h2 fontOutLine mainColor">A</li>
						<li><hr></li>
						<li>はい、対応できます。</li>
					</ul>
				
				</dd>
			</dl>
		</div>
	</div>
</section>

<section class="topNews padding bgSubColor" id="news">
	<div class="container">
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">お知らせ</h3>
				<p class="mainColor fontEn h4">News</p>
			</div>
		</div>
		<div class="topNewsBox mb50" data-aos="fade-up">
		
			<?php
				//$paged = (get_query_var('page')) ? get_query_var('page') : 1;
				$paged = get_query_var('page');
				$args = array(
					'post_type' =>  'post', // 投稿タイプを指定
					'paged' => $paged,
					'posts_per_page' => 6, // 表示するページ数
					'orderby'=>'date',
					/*'cat' => -5,*/
					'order'=>'DESC'
							);
				$wp_query = new WP_Query( $args ); // クエリの指定 	
				while ( $wp_query->have_posts() ) : $wp_query->the_post();
					//ここに表示するタイトルやコンテンツなどを指定 
				get_template_part('content-post-top'); 
				endwhile;
				//wp_reset_postdata(); //忘れずにリセットする必要がある
				wp_reset_query();
			?>		
		</div>
		<a href="<?php echo home_url();?>/news" class="button white tra text-center">詳しく見る</a>
	</div>
</section>


<section class="topCompany margin" id="company">
	<div class="container">
		<div class="text-center mb50">
			<div class="inlineBlock">
				<h3 class="h3 bold titleBd mb10">会社概要</h3>
				<p class="mainColor fontEn h4">Company</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6" data-aos="fade-right">
				<img src="<?php echo get_template_directory_uri();?>/img/cp_09.jpg" alt="">
			</div>
			<div class="col-sm-6" data-aos="fade-left">
				<div class="topCompanyDl">
					<dl>
						<dt>社名</dt>
						<dd>F・パートナーズ株式会社</dd>
						<dt>設立</dt>
						<dd>平成30年1月5日</dd>
						<dt>資本金</dt>
						<dd>100万円</dd>
						<dt>事業内容</dt>
						<dd>
						
・コンサルティング事業<br>
・売上向上支援事業<br>
・人材採用支援事業<br>
・デザイン事業<br>
・WEBサイト制作事業<br>
・広告代理店事業<br>
・ビジネスマッチング事業　他						
						</dd>
						<dt>支援実績</dt>
						<dd>小売業　サービス業　製造業　土木建築業　飲食業　運送業　士業　医業など　法人・個人事業者50社以上</dd>
						<dt>保有資格</dt>
						<dd>経営士　一級販売士　経営支援アドバイザー　税理士　行政書士　ファイナンシャルプランニング技能士　カラーコーディネーター　MOS　情報処理技術検定　etc</dd>
						<dt>主要銀行</dt>
						<dd>中国銀行</dd>
						<dt>所在地</dt>
						<dd>岡山市北区大安寺南町2-11-46（渋谷典彦税理士事務所内）</dd>
						<dt>電話番号</dt>
						<dd>0863-33-1928</dd>
					</dl>
				</div>
			</div>
		</div>
	</div>
</section>

<iframe class="company_map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d25641.475204260067!2d133.87563231924793!3d34.66590480866236!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x35540715d1f12c03%3A0x1522f37a9456a3e4!2z44CSNzAwLTAwNjQg5bKh5bGx55yM5bKh5bGx5biC5YyX5Yy65aSn5a6J5a-65Y2X55S677yS5LiB55uu77yR77yR4oiS77yU77yW!5e0!3m2!1sja!2sjp!4v1550214724818" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>

<section class="margin" data-aos="fade-up">
	<div class="container">
		<ul class="autoplaySlick">
			<li><img class="mb10" src="<?php echo get_template_directory_uri();?>/img/fp_12.jpg" alt=""></li>
			<li><img class="mb10" src="<?php echo get_template_directory_uri();?>/img/fp_13.jpg" alt=""></li>
			<li><img class="mb10" src="<?php echo get_template_directory_uri();?>/img/fp_14.jpg" alt=""></li>
			<li><img class="mb10" src="<?php echo get_template_directory_uri();?>/img/fp_15.jpg" alt=""></li>
			<li><img class="mb10" src="<?php echo get_template_directory_uri();?>/img/fp_16.jpg" alt=""></li>
		</ul>
	</div>
</section>


<script>

$('.autoplaySlick').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 2000,
	prevArrow: '<div class="slider-arrow slider-prev fa fa-angle-left"></div>',
	nextArrow: '<div class="slider-arrow slider-next fa fa-angle-right"></div>',
	
responsive: [
	{
		breakpoint: 768, //767px以下のサイズに適用
		settings: {
			slidesToShow:1
		}
	}
]	
});
</script>


<style>/*
<section class="topNews">
	<div class="container">
		<div class="row">
			<div class="col-sm-4" data-aos="fade-right">
				<p class="fontEn h3 titleLine mb10">News</p>
				<h3 class="h3 bold mb30 mainColor">お知らせ</h3>
				<a href="<?php echo home_url();?>/news" class="mb30 button bold mainColor tra text-center ml0 pc">もっと見る</a>
			</div>
			<div class="col-sm-8" data-aos="fade-left">
				<ul class="topNewsUl mb30">
				
		
					<?php
						//$paged = (get_query_var('page')) ? get_query_var('page') : 1;
						$paged = get_query_var('page');
						$args = array(
							'post_type' =>  'post', // 投稿タイプを指定
							'paged' => $paged,
							'posts_per_page' => 3, // 表示するページ数
							'orderby'=>'date',
							/*'cat' => -5,*/
							'order'=>'DESC'
									);
						$wp_query = new WP_Query( $args ); // クエリの指定 	
						while ( $wp_query->have_posts() ) : $wp_query->the_post();
							//ここに表示するタイトルやコンテンツなどを指定 
						get_template_part('content-post-top'); 
						endwhile;
						//wp_reset_postdata(); //忘れずにリセットする必要がある
						wp_reset_query();
					?>		
		
				
				
				</ul>
			</div>
		</div>
		<a href="<?php echo home_url();?>/news" class="mb30 button bold mainColor tra text-center ml0 sp">もっと見る</a>
	</div>
</section>
*/</style>


</main>



<?php get_footer(); ?>