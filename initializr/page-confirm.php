<?php get_header(); ?>

<main>

<section class="padding bgMainColor">
	<div class="container">
		<div class="text-center">
			<div class="inlineBlock white">
				<h3 class="h3 bold titleBdWhite mb10">確認画面</h3>
				<p class="fontEn h4">Confirm</p>
			</div>
		</div>
	</div>
</section>



<section class="margin">
	<div class="container">
		<div class="row">
			<div class="contInCont" data-aos="fade-up">
				<div class="mb30 text-center width780">
					<p>こちらの内容でお間違いないでしょうか？</p>
					<p>問題なければ下記の送信ボタンを押して下さい。</p>
				</div>
				<a class="telLink fontEn h0 text-center bold mainColor block mb30" href="tel:0863331928">0863-33-1928</a>
				<div class="contactForm" data-aos="fade-up"><?php echo do_shortcode('[mwform_formkey key="22"]'); ?></div>
			</div>
		</div>
	</div>
</section>


<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>