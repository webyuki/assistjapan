<?php
/*
Template Name: わたしたちのできること
*/
?>

<?php get_header(); ?>
<div class="pageFv">
	<div class="container">
		<?php get_template_part( 'parts/page-fv' ); ?>
	</div>
</div>
<?php $slug = $post -> post_name; ?>

<section class="area_single">
	<div class="container">
		<?php get_template_part( 'parts/breadcrumb' ); ?>				
		<div class="row">
			<div class="col-sm-9">
				<div class="entry">
					<?php
						$paged = get_query_var('paged');
						query_posts($query_string . '&posts_per_page=1&paged=' . $paged);
						while ( have_posts() ) : the_post();
							get_template_part('content','post'); 
						endwhile ;
					?>		
				<?php get_template_part( 'parts/postlink' ); ?>				
					
				</div>
				<?php //get_template_part( 'parts/pagenation' ); ?>				
			</div>
			<div class="col-sm-3">
				<?php get_sidebar(); ?>
   			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>




