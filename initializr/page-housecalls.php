<?php get_header(); ?>
<main>
<section class="pageHeader bgCenter mb30">
	<div class="pageHeaderCover">
		<div class="container">
			<div class="text-center white pageHeaderTitle">
				<h3 class="titleJp h3 titleBd"><?php the_title();?></h3>
				<p class="titleEn questrial"><?php $post = get_page($page_id); echo $post->post_name;?></p>
			</div>
		</div>
	</div>
</section>
<section class="pageNavi mb50">
	<div class="container">
		<ul class="inline_block pageNaviUl text-center white tra text_m">
			<li><a href="#whathousecalls">往診サービスとは？</a>
			<li><a href="#probrem">こんな方にオススメ</a>
			<li><a href="#oshindetail">施術内容</a>
			<li><a href="#area">施術エリア</a>
			<li><a href="#fee">料 金</a>
			</li>
		</ul>
	</div>
</section>

<section class="mb50" id="whathousecalls">
	<div class="container">
		<div class="text-center mb50">
			<h3 class="titleJp mColor h3 titleBd">往診サービスとは？</h3>
			<p class="titleEn subColor questrial">What's house calls?</p>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<h4 class="h4 bold mb30 text-center">整骨院に通院が必要な方、柔道整復師・鍼灸師によるサポートが必要な方などで、<br>痛みにより外出が困難、交通手段がなく通院できない方などに対して<br>厚生労働大臣免許を持つ、経験豊かな施術師がご自宅に伺い施術させていただきます。<br>また、ご家族にサポートが必要な方もお気軽にご相談ください。</h4>
			</div>
		</div>
		
	</div>
</section>

<section class="sectionCheck100" id="probrem">
	<div class="container">
		<div class="text-center mb50">
			<h3 class="titleJp mColor h3 titleBd">こんな方におススメ</h3>
			<p class="titleEn subColor questrial">PROBREM</p>
		</div>
		<div class="checkWith100 max700" data-aos="fade-up">
			<ul class="inline_block footerAppealCheck h4 bold">
				<li><div class="imgCircle bgCenter" style="background-image:url('<?php echo get_template_directory_uri();?>/img/topprobrem.jpg');"></div><i class="fa fa-check-circle"></i>ご自宅でリハビリに取り組まれている方</li>
				<li><div class="imgCircle bgCenter" style="background-image:url('<?php echo get_template_directory_uri();?>/img/topprobrem02.jpg');"></div><i class="fa fa-check-circle"></i>寝たきりの方</li>
				<li><div class="imgCircle bgCenter" style="background-image:url('<?php echo get_template_directory_uri();?>/img/topprobrem03.jpg');"></div><i class="fa fa-check-circle"></i>移動手段がない方</li>
				<li><div class="imgCircle bgCenter" style="background-image:url('<?php echo get_template_directory_uri();?>/img/topprobrem04.jpg');"></div><i class="fa fa-check-circle"></i>移動時間の確保が難しい方</li>
				<li><div class="imgCircle bgCenter" style="background-image:url('<?php echo get_template_directory_uri();?>/img/topprobrem05.jpg');"></div><i class="fa fa-check-circle"></i>自宅での治療を希望される方</li>
			</ul>
		</div>
		
	</div>
</section>


<section class="topStrong bgImgBlur relative mb50" id="oshindetail">
	<div class="whiteBg">
		<div class="container">
			<div class="text-center">
				<h3 class="titleJp h3 titleBd">施術内容</h3>
			</div>
			<div class="row" data-aos="fade-up">
				<div class="col-sm-3">
					<div class="point3 matchHeight">
						<div class="point3Number flex justCenter alignCenter">
							<div class="h3 italic mColor bold">01</div>
						</div>
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/housecallsservice01.jpg" alt="手技療法">
						<h4 class="h4 bold text-center point3TitleBd">手技療法</h4>
						<p class="text_m point3Text">緊張をとり除き、筋肉や関節の動きを調整することで、体の痛みや不快感を緩和させます。</p>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="point3 matchHeight">
						<div class="point3Number flex justCenter alignCenter">
							<div class="h3 italic mColor bold">02</div>
						</div>
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/house_service_02.jpg" alt="リハビリ">
						<h4 class="h4 bold text-center point3TitleBd">リハビリ</h4>
						<p class="text_m point3Text">電気療法と手技療法をおこない、自分で歩けるように、自分のことは自分でできるようになっていただくためのリハビリをおこないます。</p>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="point3 matchHeight">
						<div class="point3Number flex justCenter alignCenter">
							<div class="h3 italic mColor bold">03</div>
						</div>
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/service03.jpg" alt="鍼灸療法">
						<h4 class="h4 bold text-center point3TitleBd">鍼灸療法</h4>
						<p class="text_m point3Text">鍼灸療法は人間が本来持つ自然治癒力を高め、様々な症状を改善させます。</p>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="point3 matchHeight">
						<div class="point3Number flex justCenter alignCenter">
							<div class="h3 italic mColor bold">04</div>
						</div>
						<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/service_other_01.jpg" alt="治療機器を使った療法">
						<h4 class="h4 bold text-center point3TitleBd">治療機器を使った療法</h4>
						<p class="text_m point3Text">症状に最適な治療機器を用い、症状を改善させます。</p>
					</div>
				</div>
				
			</div>
		</div>
	
	</div>
</section>
<section class="sectionFlow mb50">
	<div class="container">
		<div class="text-center mb50">
			<h3 class="titleJp mColor h3 titleBd">施術の流れ</h3>
			<p class="titleEn subColor questrial">FLOW</p>
		</div>
		<div class="row">
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative clearfix floatItems">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">01</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/flow01.jpg" alt="まずはお電話ください。ご自宅に伺う日程を調整いたします">
					<p class="mb10">まずはお電話ください。ご自宅に伺う日程を調整いたします</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative clearfix floatItems">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">02</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/houseflow02.jpg" alt="往診日。施術師がご自宅に伺います。">
					<p class="mb10">往診日。施術師がご自宅に伺います。<br><span class="text_m">※車で伺うため駐車場がない場合、その旨、電話予約時にお知らせください</span></p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative clearfix floatItems">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">03</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/houseflow03.jpg" alt="施術をおこないます。">
					<p class="mb10">施術をおこないます。施術時間は症状、施術内容により異なりますが30分程度です</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="topMenuItems matchHeight relative clearfix floatItems">
					<div class="h3 italic mColor bold sectionFlowNumber absolute">04</div>
					<img class="mb10" src="<?php echo get_template_directory_uri();?>/img/recruit2.jpg" alt="施術終了後、次回訪問日を決めます。">
					<p class="mb10">施術終了後、次回訪問日を決めます。症状が解消するまでしっかりサポートいたします</p>
				</div>
			</div>
		
		</div>
	</div>
</section>
<section class="houseArea mb50" id="area">
	<div class="container">
		<div class="text-center mb50">
			<h3 class="titleJp mColor h3 titleBd">対応エリア</h3>
			<p class="titleEn subColor questrial">AREA</p>
			<h4 class="h4 bold mb30 text-center">総社市全域対応致しますので、是非お声がけ下さい。</h4>
			<div class="max700" data-aos="fade-up">
				<!--
				<ul class="text_m inline_block areaTextTag">
					<li>総社市	赤浜（あかはま）</li>
					<li>総社市井尻野（いじりの）</li>
					<li>総社市	井手（いで）</li>
					<li>総社市	宇山（うやま）</li>
					<li>総社市	駅前（えきまえ）</li>
					<li>総社市	岡谷（おかだに）</li>
					<li>総社市	奥坂（おくさか）</li>
					<li>総社市	刑部（おしかべ）</li>
					<li>総社市	影（かげ）</li>
					<li>総社市	金井戸（かないど）</li>
					<li>総社市	上林（かんばやし）</li>
					<li>総社市	上原（かんばら）</li>
					<li>総社市	北溝手（きたみぞて）</li>
					<li>総社市	清音（きよね）</li>
					<li>総社市	久代（くしろ）</li>
					<li>総社市	窪木（くぼき）</li>
					<li>総社市	久米（くめ）</li>
					<li>総社市	黒尾（くろお）</li>
					<li>総社市	槁（けやき）</li>
					<li>総社市	小寺（こでら）</li>
					<li>総社市	宍粟（しさわ）</li>
					<li>総社市	下倉（したぐら）</li>
					<li>総社市	下林（しもばやし）</li>
					<li>総社市	下原（しもばら）</li>
					<li>総社市	宿（しゅく）</li>
					<li>総社市	新本（しんぽん）</li>
					<li>総社市	地頭片山（じとうかたやま）</li>
					<li>総社市	総社（そうじゃ）</li>
					<li>総社市	種井（たねい）</li>
					<li>総社市	中央（ちゅうおう）</li>
					<li>総社市	富原（とんばら）</li>
					<li>総社市	中尾（なかお）</li>
					<li>総社市	中原（なかばら）</li>
					<li>総社市	長良（ながら）</li>
					<li>総社市	西阿曽（にしあぞ）</li>
					<li>総社市	西郡（にしごおり）</li>
					<li>総社市	西坂台（にしさかだい）</li>
					<li>総社市	延原（のぶはら）</li>
					<li>総社市	秦（はだ）</li>
					<li>総社市	原（はら）</li>
					<li>総社市	東阿曽（ひがしあぞ）</li>
					<li>総社市	日羽（ひわ）</li>
					<li>総社市	福井（ふくい）</li>
					<li>総社市	福谷（ふくたに）</li>
					<li>総社市	真壁（まかべ）</li>
					<li>総社市	槇谷（まきだに）</li>
					<li>総社市	三須（みす）</li>
					<li>総社市	溝口（みぞくち）</li>
					<li>総社市	美袋（みなぎ）</li>
					<li>総社市	南溝手（みなみみぞて）</li>
					<li>総社市	見延（みのべ）</li>
					<li>総社市	三輪（みわ）</li>
					<li>総社市	門田（もんで）</li>
					<li>総社市	八代（やしろ）</li>
					<li>総社市	山田（やまだ）</li>			
				</ul>
				-->
				<ul class="text_m inline_block areaTextTag">
					<li>総社市	赤浜</li>
					<li>総社市井尻野</li>
					<li>総社市	井手</li>
					<li>総社市	宇山</li>
					<li>総社市	駅前</li>
					<li>総社市	岡谷</li>
					<li>総社市	奥坂</li>
					<li>総社市	刑部</li>
					<li>総社市	影</li>
					<li>総社市	金井戸</li>
					<li>総社市	上林</li>
					<li>総社市	上原</li>
					<li>総社市	北溝手</li>
					<li>総社市	清音</li>
					<li>総社市	久代</li>
					<li>総社市	窪木</li>
					<li>総社市	久米</li>
					<li>総社市	黒尾</li>
					<li>総社市	槁</li>
					<li>総社市	小寺</li>
					<li>総社市	宍粟</li>
					<li>総社市	下倉</li>
					<li>総社市	下林</li>
					<li>総社市	下原</li>
					<li>総社市	宿</li>
					<li>総社市	新本</li>
					<li>総社市	地頭片山</li>
					<li>総社市	総社</li>
					<li>総社市	種井</li>
					<li>総社市	中央</li>
					<li>総社市	富原</li>
					<li>総社市	中尾</li>
					<li>総社市	中原</li>
					<li>総社市	長良</li>
					<li>総社市	西阿曽</li>
					<li>総社市	西郡</li>
					<li>総社市	西坂台</li>
					<li>総社市	延原</li>
					<li>総社市	秦</li>
					<li>総社市	原</li>
					<li>総社市	東阿曽</li>
					<li>総社市	日羽</li>
					<li>総社市	福井</li>
					<li>総社市	福谷</li>
					<li>総社市	真壁</li>
					<li>総社市	槇谷</li>
					<li>総社市	三須</li>
					<li>総社市	溝口</li>
					<li>総社市	美袋</li>
					<li>総社市	南溝手</li>
					<li>総社市	見延</li>
					<li>総社市	三輪</li>
					<li>総社市	門田</li>
					<li>総社市	八代</li>
					<li>総社市	山田</li>			
				</ul>
			</div>

		</div>
	</div>
</section>

<div class="section mb100" id="fee">
	<div class="container">
		<div class="text-center mb50">
			<h3 class="titleJp mColor h3 titleBd">往診サービスの料金</h3>
			<p class="titleEn subColor questrial mb30">FEE</p>
			<p class="bold">「患者様の症状、お悩みを受け止め、1日でも早く回復されるよう最善の施術を提供する」</p>
			<p class="mb30">この当たり前とも思われてることができていない医療機関が悲しいですが増えてきています。<br>
今回、多くの方に当院の施術を受けて頂きたいと思い、キャンペーンを実施することにしました。</p>
			<table class="table table-borderd topFeeDl " data-aos="fade-up">
				<tbody>
					<tr>
						<th class="h4">1回</th>
						<td class="text_m"><span class="italic h2 questrial bold">7,500</span>円〜</td>
					</tr>
				</tbody>
			</table>
			<img class="feeArrowBottom" src="<?php echo get_template_directory_uri();?>/img/arrowbottom.png" alt="">
			<p class="titleJp mColor h3 titleBd text-center FeeBd mb30">＼今なら<span class="h1">初回お試しキャンペーン</span>で、<span class="italic h0 bold questrial">2,980</span>円／</p>
			<p>※ご予約時に「初回お試しキャンペンを見た」とお伝えください</p>
	</div>
</div>







<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>