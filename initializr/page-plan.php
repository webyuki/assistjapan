<?php get_header(); ?>
<main>


<section class="pagePlanFv pageHeader mb50" style="background-image:url('<?php echo get_template_directory_uri();?>/img/plan_fv.jpg');">
	<div class="pageHeaderWhite">
		<div class="pageHeaderText text-center">
			<div class="container">
				<h3 class="klee h1 mainColor">施工プラン</h3>
				<p class="titleEn fontEn h1 bold subColor">PLAN</p>
			</div>
		</div>
		<div class="pageHeaderCircle" style="background-image:url('<?php echo get_template_directory_uri();?>/img/plan_fv.jpg');"></div>
	</div>
</section>

<section class="pageAboutGreet mb50">
	<div class="container">
		<div class="titleBg titleBgColor text-center mb10">
			<p class="titleEn fontEn h1 bold mainColor">THINKING</p>
			<h3 class="titleJp h4 mainColor">お客様に合ったニーズをご提案</h3>
		</div>
		<div class="titleBgWave titleBgWaveColor mb30"></div>
		<p class="text-center with780 mb50">大和建装では、お問い合わせから施工、
アフターフォローまですべての工程を一貫して対応しております。
そのため、無駄のない料金体系で、上質なサービスを提供いたします。
また、施工者が責任を持ってお客様の声をお聞きいたしますので、
安心・納得のうえ、ご要望に応じることが可能です。</p>
		<div class="row mb50">
			<div class="col-sm-4">
				<a href="">
					<div class="topPlanBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_plan_01.jpg');">
						<div class="topPlanBoxWhite text-center">
							<img class="topPlanBoxIco" src="<?php echo get_template_directory_uri();?>/img/top_plan_ico_01.png" alt="">
							<p class="titleEn fontEn h1 bold mainColor">long wearing</p>
							<h4 class="titleJp h5 mainColor bold mb10">長持ち高耐久塗装プラン</h4>
							<p class="text_m">10年以上お住まいのお家で暮らす方におすすめです。</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-4">
				<a href="">
					<div class="topPlanBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_plan_01.jpg');">
						<div class="topPlanBoxWhite text-center">
							<img class="topPlanBoxIco" src="<?php echo get_template_directory_uri();?>/img/top_plan_ico_01.png" alt="">
							<p class="titleEn fontEn h1 bold mainColor">long wearing</p>
							<h4 class="titleJp h5 mainColor bold mb10">長持ち高耐久塗装プラン</h4>
							<p class="text_m">10年以上お住まいのお家で暮らす方におすすめです。</p>
						</div>
					</div>
				</a>
			</div>
			<div class="col-sm-4">
				<a href="">
					<div class="topPlanBox mb30" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_plan_01.jpg');">
						<div class="topPlanBoxWhite text-center">
							<img class="topPlanBoxIco" src="<?php echo get_template_directory_uri();?>/img/top_plan_ico_01.png" alt="">
							<p class="titleEn fontEn h1 bold mainColor">long wearing</p>
							<h4 class="titleJp h5 mainColor bold mb10">長持ち高耐久塗装プラン</h4>
							<p class="text_m">10年以上お住まいのお家で暮らす方におすすめです。</p>
						</div>
					</div>
				</a>
			</div>
		</div>

	</div>
	
</section>

<section class="pagePlanDetail" style="background-image:url('<?php echo get_template_directory_uri();?>/img/plan_think_bg_01.jpg');">
	<div class="pagePlanDetailWhite text-center">
		<div class="container">
			<img class="topPlanBoxIco" src="<?php echo get_template_directory_uri();?>/img/top_plan_ico_01.png" alt="">
			<p class="titleEn fontEn h0 bold subColor">long wearing</p>
			<h4 class="titleJp h3 mainColor bold mb10">長持ち高耐久塗装プラン</h4>
			<p>大和建装では、お問い合わせから施工、<br>
		アフターフォローまですべての工程を一貫して対応しております。<br>
		そのため、無駄のない料金体系で、上質なサービスを提供いたします。</p>
			<p class="subColor mb30 h0 pagePlanDetailFee">50,000<span class="h3">円　〜</span>　100,000<span class="h3">円</span></p>
			<p class="h4 bold pagePlanDetailBlue mb10">こんな方にオススメ</p>
			<ul class="pagePlanDetailUl bold h4">
				<li><i class="fa fa-check"></i>10年以上今の家に住む予定だ</li>
				<li><i class="fa fa-check"></i>10年以上今の家に住む予定だ</li>
				<li><i class="fa fa-check"></i>10年以上今の家に住む予定だ</li>
				<li><i class="fa fa-check"></i>10年以上今の家に住む予定だ</li>
			</ul>
		</div>
	</div>
</section>
<section class="pagePlanDetail" style="background-image:url('<?php echo get_template_directory_uri();?>/img/plan_think_bg_02.jpg');">
	<div class="pagePlanDetailWhite text-center">
		<div class="container">
			<img class="topPlanBoxIco" src="<?php echo get_template_directory_uri();?>/img/top_plan_ico_01.png" alt="">
			<p class="titleEn fontEn h0 bold subColor">long wearing</p>
			<h4 class="titleJp h3 mainColor bold mb10">長持ち高耐久塗装プラン</h4>
			<p>大和建装では、お問い合わせから施工、<br>
		アフターフォローまですべての工程を一貫して対応しております。<br>
		そのため、無駄のない料金体系で、上質なサービスを提供いたします。</p>
			<p class="subColor mb30 h0 pagePlanDetailFee">50,000<span class="h3">円　〜</span>　100,000<span class="h3">円</span></p>
			<p class="h4 bold pagePlanDetailBlue mb10">こんな方にオススメ</p>
			<ul class="pagePlanDetailUl bold h4">
				<li><i class="fa fa-check"></i>10年以上今の家に住む予定だ</li>
				<li><i class="fa fa-check"></i>10年以上今の家に住む予定だ</li>
				<li><i class="fa fa-check"></i>10年以上今の家に住む予定だ</li>
				<li><i class="fa fa-check"></i>10年以上今の家に住む予定だ</li>
			</ul>
		</div>
	</div>
</section>
<section class="pagePlanDetail" style="background-image:url('<?php echo get_template_directory_uri();?>/img/plan_think_bg_03.jpg');">
	<div class="pagePlanDetailWhite text-center">
		<div class="container">
			<img class="topPlanBoxIco" src="<?php echo get_template_directory_uri();?>/img/top_plan_ico_01.png" alt="">
			<p class="titleEn fontEn h0 bold subColor">long wearing</p>
			<h4 class="titleJp h3 mainColor bold mb10">長持ち高耐久塗装プラン</h4>
			<p>大和建装では、お問い合わせから施工、<br>
		アフターフォローまですべての工程を一貫して対応しております。<br>
		そのため、無駄のない料金体系で、上質なサービスを提供いたします。</p>
			<p class="subColor mb30 h0 pagePlanDetailFee">50,000<span class="h3">円　〜</span>　100,000<span class="h3">円</span></p>
			<p class="h4 bold pagePlanDetailBlue mb10">こんな方にオススメ</p>
			<ul class="pagePlanDetailUl bold h4">
				<li><i class="fa fa-check"></i>10年以上今の家に住む予定だ</li>
				<li><i class="fa fa-check"></i>10年以上今の家に住む予定だ</li>
				<li><i class="fa fa-check"></i>10年以上今の家に住む予定だ</li>
				<li><i class="fa fa-check"></i>10年以上今の家に住む予定だ</li>
			</ul>
		</div>
	</div>
</section>


<section class="topAbout relative pageAboutHosp mb100">
	<svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="absolute jsWave"><path id="wave" d=""/></svg>
	<div class="container">
		<div class="titleBg text-center mb10">
			<p class="titleEn fontEn h1 bold mainColor">Others</p>
			<h3 class="titleJp h4 mainColor">塗装以外のサービスも承っております</h3>
		</div>
		<div class="titleBgWave mb30"></div>
		<p class="text-center with780 mb50">大和建装では、お問い合わせから施工、
アフターフォローまですべての工程を一貫して対応しております。
そのため、無駄のない料金体系で、上質なサービスを提供いたします。
また、施工者が責任を持ってお客様の声をお聞きいたしますので、
安心・納得のうえ、ご要望に応じることが可能です。</p>
		<div class="flex justCenter">
			<div class="pageAboutHospBox">
				<img class="topPlanBoxIco pageAboutHospBoxIco" src="<?php echo get_template_directory_uri();?>/img/top_plan_ico_01.png" alt="">
				<h4 class="mainColor bold mb30 h4 text-center pageAboutHospBoxBorder">外装工事</h4>
				<p class="text_m mainColor">大和建装では、お問い合わせから施工、アフターフォローまですべての工程を一貫して対応しております。</p>
			</div>
			<div class="pageAboutHospBox">
				<img class="topPlanBoxIco pageAboutHospBoxIco" src="<?php echo get_template_directory_uri();?>/img/top_plan_ico_01.png" alt="">
				<h4 class="mainColor bold mb30 h4 text-center pageAboutHospBoxBorder">外装工事</h4>
				<p class="text_m mainColor">大和建装では、お問い合わせから施工、アフターフォローまですべての工程を一貫して対応しております。</p>
			</div>
			<div class="pageAboutHospBox">
				<img class="topPlanBoxIco pageAboutHospBoxIco" src="<?php echo get_template_directory_uri();?>/img/top_plan_ico_01.png" alt="">
				<h4 class="mainColor bold mb30 h4 text-center pageAboutHospBoxBorder">外装工事</h4>
				<p class="text_m mainColor">大和建装では、お問い合わせから施工、アフターフォローまですべての工程を一貫して対応しております。</p>
			</div>
			<div class="pageAboutHospBox">
				<img class="topPlanBoxIco pageAboutHospBoxIco" src="<?php echo get_template_directory_uri();?>/img/top_plan_ico_01.png" alt="">
				<h4 class="mainColor bold mb30 h4 text-center pageAboutHospBoxBorder">外装工事</h4>
				<p class="text_m mainColor">大和建装では、お問い合わせから施工、アフターフォローまですべての工程を一貫して対応しております。</p>
			</div>
		</div>
		<div class="flex justCenter">
			<div class="pageAboutHospBox">
				<img class="topPlanBoxIco pageAboutHospBoxIco" src="<?php echo get_template_directory_uri();?>/img/top_plan_ico_01.png" alt="">
				<h4 class="mainColor bold mb30 h4 text-center pageAboutHospBoxBorder">外装工事</h4>
				<p class="text_m mainColor">大和建装では、お問い合わせから施工、アフターフォローまですべての工程を一貫して対応しております。</p>
			</div>
			<div class="pageAboutHospBox">
				<img class="topPlanBoxIco pageAboutHospBoxIco" src="<?php echo get_template_directory_uri();?>/img/top_plan_ico_01.png" alt="">
				<h4 class="mainColor bold mb30 h4 text-center pageAboutHospBoxBorder">外装工事</h4>
				<p class="text_m mainColor">大和建装では、お問い合わせから施工、アフターフォローまですべての工程を一貫して対応しております。</p>
			</div>
			<div class="pageAboutHospBox">
				<img class="topPlanBoxIco pageAboutHospBoxIco" src="<?php echo get_template_directory_uri();?>/img/top_plan_ico_01.png" alt="">
				<h4 class="mainColor bold mb30 h4 text-center pageAboutHospBoxBorder">外装工事</h4>
				<p class="text_m mainColor">大和建装では、お問い合わせから施工、アフターフォローまですべての工程を一貫して対応しております。</p>
			</div>
			<div class="pageAboutHospBox">
				<img class="topPlanBoxIco pageAboutHospBoxIco" src="<?php echo get_template_directory_uri();?>/img/top_plan_ico_01.png" alt="">
				<h4 class="mainColor bold mb30 h4 text-center pageAboutHospBoxBorder">外装工事</h4>
				<p class="text_m mainColor">大和建装では、お問い合わせから施工、アフターフォローまですべての工程を一貫して対応しております。</p>
			</div>
		</div>
	</div>
	<svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="absolute jsWaveBottom"><path id="waveBottom" d=""/></svg>
</section>

<section class="pageAboutGreet mb50 pagePlanTips">
	<div class="container">
		<div class="titleBg titleBgColor text-center mb10">
			<p class="titleEn fontEn h1 bold mainColor">TIPS</p>
			<h3 class="titleJp h4 mainColor">お役立ち情報</h3>
		</div>
		<div class="titleBgWave titleBgWaveColor mb30"></div>
		<div class="row">
			<div class="col-sm-4">
				<div class="pagePlanTipsBox mb30">
					<div class="pagePlanTipsBoxImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_work_01.jpg');"></div>
					<div class="pagePlanTipsBoxText">
						<p class="h5 mainColor bold mb10">建物を美しく保つための塗料とは？</p>
						<p class="pagePlanTipsBoxTextLi text_s"><span class="pagePlanTipsBoxTextLiCate white">外装工事</span><span class="pagePlanTipsBoxTextLiDate subColor">2018.10.10</span></p>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="pagePlanTipsBox mb30">
					<div class="pagePlanTipsBoxImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_work_01.jpg');"></div>
					<div class="pagePlanTipsBoxText">
						<p class="h5 mainColor bold mb10">建物を美しく保つための塗料とは？</p>
						<p class="pagePlanTipsBoxTextLi text_s"><span class="pagePlanTipsBoxTextLiCate white">外装工事</span><span class="pagePlanTipsBoxTextLiDate subColor">2018.10.10</span></p>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="pagePlanTipsBox mb30">
					<div class="pagePlanTipsBoxImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_work_01.jpg');"></div>
					<div class="pagePlanTipsBoxText">
						<p class="h5 mainColor bold mb10">建物を美しく保つための塗料とは？</p>
						<p class="pagePlanTipsBoxTextLi text_s"><span class="pagePlanTipsBoxTextLiCate white">外装工事</span><span class="pagePlanTipsBoxTextLiDate subColor">2018.10.10</span></p>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="pagePlanTipsBox mb30">
					<div class="pagePlanTipsBoxImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_work_01.jpg');"></div>
					<div class="pagePlanTipsBoxText">
						<p class="h5 mainColor bold mb10">建物を美しく保つための塗料とは？</p>
						<p class="pagePlanTipsBoxTextLi text_s"><span class="pagePlanTipsBoxTextLiCate white">外装工事</span><span class="pagePlanTipsBoxTextLiDate subColor">2018.10.10</span></p>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="pagePlanTipsBox mb30">
					<div class="pagePlanTipsBoxImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_work_01.jpg');"></div>
					<div class="pagePlanTipsBoxText">
						<p class="h5 mainColor bold mb10">建物を美しく保つための塗料とは？</p>
						<p class="pagePlanTipsBoxTextLi text_s"><span class="pagePlanTipsBoxTextLiCate white">外装工事</span><span class="pagePlanTipsBoxTextLiDate subColor">2018.10.10</span></p>
					</div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="pagePlanTipsBox mb30">
					<div class="pagePlanTipsBoxImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_work_01.jpg');"></div>
					<div class="pagePlanTipsBoxText">
						<p class="h5 mainColor bold mb10">建物を美しく保つための塗料とは？</p>
						<p class="pagePlanTipsBoxTextLi text_s"><span class="pagePlanTipsBoxTextLiCate white">外装工事</span><span class="pagePlanTipsBoxTextLiDate subColor">2018.10.10</span></p>
					</div>
				</div>
			</div>
		</div>
		<a href="" class="button buttonBgWhite mainColor tra text-center">お役立ち情報をもっと見る</a>

	</div>
	
</section>

<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>