<?php
	$category = get_the_category();
	$cat_id   = $category[0]->cat_ID;
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
?>

<!-- カテゴリーIDを表示したい所に -->
<?php //echo $cat_id; ?>

<!-- カテゴリー名を表示したい所に -->
<?php //echo $cat_name; ?>

<!-- カテゴリースラッグを表示したい所に -->
<?php //echo $cat_slug; ?>


			<a href="<?php the_permalink();?>">
				<ul class="topNewsBoxUl LiInlineBlock mb30">
					<li class="grayColor text_m"><?php the_time('y/m/d'); ?></li>
					<li class="white text-center text_m"><?php echo $cat_name; ?></li>
					<li class="bold"><?php the_title();?></li>
				</ul>
			</a>

