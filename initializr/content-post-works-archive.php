						
							
			<li class="matchHeight">
				<a href="<?php the_permalink(); ?>">
					<div class="pageWorkLiBox">
					
						<?php if (has_post_thumbnail()):?>
							<?php 
								// アイキャッチ画像のIDを取得
								$thumbnail_id = get_post_thumbnail_id();
								// mediumサイズの画像内容を取得（引数にmediumをセット）
								$eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
								$eye_img_s = wp_get_attachment_image_src( $thumbnail_id , 'thumb_size_s_false',false );
							?>
								<div class="bgImg relative mb10" style="background-image:url('<?php echo $eye_img_s[0];?>')">
							<?php else: ?>
								<div class="bgImg relative mb10" style="background-image:url('<?php echo get_template_directory_uri();?>/img/sample01.png')">
						<?php endif; ?>
						
							<?php 
								if ($terms = get_the_terms($post->ID, 'works_cate')) {
									foreach ( $terms as $term ) {
										echo '<div class="pageWorkLiBoxCate white bgMainColor absolute">' . esc_html($term->name) .'</div>';
									}
								}
							?>
							
					
						</div>
						
						
						<h3 class="mainColor h4 bold"><?php the_title(); ?></h3>
						<div class="pageWorkLiBoxDate subColor fontEn text_s"><?php echo get_the_date( 'F d, Y' ); ?></div>
					</div>
				</a>
			</li>
