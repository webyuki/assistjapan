<li>	<a class="topWorkBoxA" href="<?php the_permalink(); ?>"><?php if (has_post_thumbnail()):?><?php 
								// アイキャッチ画像のIDを取得
								$thumbnail_id = get_post_thumbnail_id();
								// mediumサイズの画像内容を取得（引数にmediumをセット）
								$eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
								$eye_img_s = wp_get_attachment_image_src( $thumbnail_id , 'thumb_size_s_false',false );
							?><div class="topWorkBox bgImg flex justCenter alignCenter relative" style="background-image:url('<?php echo $eye_img_s[0];?>')"><?php else: ?><div class="topWorkBox bgImg flex justCenter alignCenter relative" style="background-image:url('<?php echo get_template_directory_uri();?>/img/sample01.png')"><?php endif; ?><div class="topWorkBoxBefore matchHeight"><div class="text-center tra visible topWorkBoxText"><h4 class="mincho white h3 mb10"><?php the_title(); ?></h4><?php 
								if ($terms = get_the_terms($post->ID, 'works_area')) {
									foreach ( $terms as $term ) {
										echo '<h5 class="topWorkCate white bgMainColor mb10">' . esc_html($term->name) .'</h5>';
									}
								}
							?><p class="topWorkDate fontEn subColor text_m"><?php echo get_the_date( 'F d, Y' ); ?></p></div></div></div></a></li>