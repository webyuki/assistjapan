<li>
	<a href="<?php the_permalink();?>">
		<img src="<?php echo get_template_directory_uri();?>/img/sample_thumb.jpg">
		<p class="margin0 h5 bold line_height_m"><?php the_title();?></p>
	</a>
<?php
if ($terms = get_the_terms($post->ID, 'cat_example')):
    foreach ( $terms as $term ) :
        $slug = esc_html($term->slug);
        $name = esc_html($term->name);
?>
	<a class="exaTaxLink text_s" href="<?php echo home_url();?>/cat_example/<?php echo $slug;?>">#<?php echo $name;?></a>

<?php 
    endforeach;
endif;
?>
</li>

